# Marcel study project

## Database 

### Table definitions

1. enumeration

Define enumerations for data schema

|name|type|notes|
|---|---|---|
|id| bigint(20)|PK, auto_increment   |
|name|varchar(100)|NULL|
|value|varchar(100)|format: ["Value1","Value2",...]|

2. dataschema

Define data schemas for data

|name|type|notes|
|---|---|---|
|id| bigint(20)|PK, auto_increment   |
|name|varchar(100)|NULL|
|version|bigint(20)|NULL default=1|
|definition|varchar(5000)|format: [{name: ${fieldName}, fieldType: ${1/2/3/4}, enumerationId: ${enumerationId:on(FieldType=4)}},...]|

3. data

Actual data collected by application. Each is related to a data schema. 

|name|type|notes|
|---|---|---|
|id| bigint(20)|PK, auto_increment   |
|createtime|timestamp|default CURRENT_TIMESTAMP|
|lastmodifiedtime|timestamp|default CURRENT_TIMESTAMP|
|schemaid|bigint(20)|FK dataschema.id|
|data|text|format: [{name:${field1Name},value:${field1Value}},{name:${field2Name},value:${field2Value}}...]|

### DB sctipts
```
	doc/sql/model/create_db.sql
	doc/sql/model/create_tables.sql
```

## REST API
API definition in [Swagger Yaml](config/swagger/swagger.yaml) 

## Web Application:

### Site Map
   1. Index
   	* Enumeration Admin
   	* Data Admin
   	* Data Schema Admin
   2. Enumeration Admin
   	* List of current enumerations in pages
   	* Action to create new, update and delete
   3. DataSchema Admin
    * List of current data schema in pages
    * Action to create new, update and delete
   4. Data Admin
    * List of current data in pages
    * Action to create new, update and delete


## Exception handling

### Validation in model module
Model validation, like checking the JSON format, happens in the 'model' module, when objects are being created or updated. It throws a ValidationException, which is a checked exception, when validation has failed. 
ModelValidator is the class who performs all model related validations. It also contains the key of different reasons of validation failure.

### Validation in web module
When a controller catches a ValidationException, it usually uses the common handleValidationException() method to wrap the exception and re-throw a new handleValidationException, which is a RuntimeException. The error handler, ValidationExceptionHandler, catches this exception and converts error message from resources before putting them in the Model attribute for error page display

## Development Environment

### Credentials


|Application|Username|Password|
|-----------|--------|--------|
|Jenkins|raymond|BUn3s8za|
|Sonar|admin|alpha|
|vnc||alpha061|

### Docker
* Create data container 
```
	docker create -v /var/lib/mysql --name marcel-data mysql:5.6
```	
* Start/Create Database container with data volume mount from data-container
``` 
    	docker run --name marcel --volumes-from marcel-data -e MYSQL_ROOT_PASSWORD=alpha -d mysql:5.6

      // Bind to 127.0.0.1:3306
      docker run --name marcel --volumes-from marcel-data -e MYSQL_ROOT_PASSWORD=alpha -d -p 127.0.0.1:3306:3306 mysql:5.6
```
* Connect to database
```
	docker run -it --link marcel:mysql --rm mysql:5.6 sh -c 'exec mysql -h"$MYSQL_PORT_3306_TCP_ADDR" -P"$MYSQL_PORT_3306_TCP_PORT" -uroot -p"$MYSQL_ENV_MYSQL_ROOT_PASSWORD" marcel'
```
* Back up data to tar.gz:
```
    	docker run --volumes-from marcel-data -v $(pwd):/backup mysql:5.6 tar czvf /backup/marcel-data.tar.gz /var/lib/mysql
```
* Restore from tar.gz:
```
	docker run --volumes-from marcel-data -v $(pwd):/backup mysql:5.6 tar xzvf /backup/marcel-data.tar.gz
```
* Open a shell:
```
    	docker exec -t -i marcel  /bin/bash
```

## Dev environment setup
1. Run mysql in docker container
```
docker create -v /var/lib/mysql --name marcel-data mysql:5.6
docker run --name marcel --volumes-from marcel-data -e MYSQL_ROOT_PASSWORD=alpha -d -p 127.0.0.1:3306:3306 mysql:5.6
```
2. Add hosts file entry on Dev machine

127.0.0.1 marcel marcel-api

3. Initialize datasbase
```
mysql -uroot -palpha -hmarcel < config/sql/model/create_db.sql
mysql -umarcel -pmarcel -hmarcel marcel < config/sql/model/create_table_and_data.sql
```

4. Build project
mvn install

5. Run REST API service
cd rest ; mvn exec:java

6. Verify in browser

http://localhost:1973/api/1/schema/

7. Run react app
```
cd marcel-react
npm install
npm start
```