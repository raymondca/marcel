package com.raymondchen.marcel.web.util;

public class PageValidationException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PageValidationException(Throwable t) {
		super(t);
	}
}
