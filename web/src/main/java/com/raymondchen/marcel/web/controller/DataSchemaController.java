package com.raymondchen.marcel.web.controller;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.raymondchen.marcel.model.DataSchema;
import com.raymondchen.marcel.model.Enumeration;
import com.raymondchen.marcel.model.exception.ObjectNotFoundException;
import com.raymondchen.marcel.model.exception.ValidationException;
import com.raymondchen.marcel.model.service.DataSchemaService;
import com.raymondchen.marcel.model.service.EnumerationService;
import com.raymondchen.marcel.model.util.FieldType;
import com.raymondchen.marcel.model.validation.ModelValidator;
import com.raymondchen.marcel.web.util.PageMessage;
import com.raymondchen.marcel.web.util.PageValidationException;

/**
 * Data Schema Admin controller
 * @author Raymond
 *
 */
@Controller
public class DataSchemaController extends BaseController  {
	
	public static final String KEY_DATA_SCHEMA_CREATION_SUCCEEDED="dataSchemaCreationSucceeded";
	public static final String KEY_DATA_SCHEMA_CREATION_FAILED="dataSchemaCreationFailed";
	public static final String KEY_DATA_SCHEMA_DELETION_SUCCEEDED="dataSchemaDeletionSucceeded";
	public static final String KEY_DATA_SCHEMA_UPDATE_SUCCEEDED="dataSchemaUpdateSucceeded";
	
	public static final String KEY_ERROR_DATA_SCHEMA_NAME_EXIST=ModelValidator.KEY_ERROR_DATA_SCHEMA_NAME_EXIST;
	public static final String KEY_ERROR_DATA_SCHEMA_FIELD_NAME_HAS_DUPLICATE=ModelValidator.KEY_ERROR_DATA_SCHEMA_FIELD_NAME_HAS_DUPLICATE;
	public static final String KEY_ERROR_DATA_SCHEMA_NAME_OR_DEFINITION_EMPTY=ModelValidator.KEY_ERROR_DATA_SCHEMA_NAME_OR_DEFINITION_EMPTY;
	
	@Autowired
	private EnumerationService enumerationService;
	
	@Autowired
	private DataSchemaService dataSchemaService; 

	/**
	 * URL of "/"
	 * @param name
	 * @param model
	 * @return the template name
	 */
	@RequestMapping(method={RequestMethod.GET},path={"/schema/index","/schema"})
	public String index( @RequestParam(value="page", required=false, defaultValue="1") int page, @RequestParam(value="pageSize", required=false, defaultValue="50") int pageSize, Model model) {
		model.addAttribute("page",page<0?0:page);
		model.addAttribute("pageSize",pageSize);
		Page<DataSchema> dataset=dataSchemaService.findAllInPage(page,pageSize);
		model.addAttribute("dataset",dataset);
		return "/schema/index";
	}
	
	@RequestMapping(path={"/schema/create"})
	public String startCreate( Model model) {
		List<Enumeration> dsList=enumerationService.list(1,Integer.MAX_VALUE);
		model.addAttribute("enumerationList",dsList);
		model.addAttribute("fieldTypes",FieldType.values());
		return "/schema/create";
	}
	
	@RequestMapping(method={RequestMethod.POST},path={"/schema"})
	public String create(@RequestParam("name") String name ,@RequestParam("definition") String definition, Model model, HttpSession session) {
		DataSchema newDataSchema=new DataSchema();
		newDataSchema.setName(name);
		newDataSchema.setDefinition(definition);
		newDataSchema.setVersion(1L);
		DataSchema insertedDataSchema=null;
		try {
			insertedDataSchema = dataSchemaService.create(newDataSchema);
		} catch (ValidationException e) {
			handleValidationException(e);
		}
		model.addAttribute(PAGE_MESSAGE_KEY,new PageMessage(getMessage(KEY_DATA_SCHEMA_CREATION_SUCCEEDED),PageMessage.Type.info));
		if (insertedDataSchema.getId()!=null) {
			session.setAttribute(PAGE_MESSAGE_KEY,new PageMessage(getMessage(KEY_DATA_SCHEMA_CREATION_SUCCEEDED),PageMessage.Type.info));
		} else {
			session.setAttribute(PAGE_MESSAGE_KEY,new PageMessage(KEY_DATA_SCHEMA_CREATION_FAILED,PageMessage.Type.error));
		}
		return "redirect:/schema/index";
	}
	
	@RequestMapping(method={RequestMethod.DELETE},path={"/schema"})
	public String delete(@RequestParam("dataSchemaId") Long dataSchemaId , Model model, HttpSession session) {
		dataSchemaService.deleteById(dataSchemaId);;
		session.setAttribute(PAGE_MESSAGE_KEY,new PageMessage(getMessage(KEY_DATA_SCHEMA_DELETION_SUCCEEDED),PageMessage.Type.info));
		return "redirect:/schema/index";
	}
	
	@RequestMapping(method={RequestMethod.GET},path={"/schema/update"})
	public String startUpdate(@RequestParam("dataSchemaId") Long dataSchemaId , Model model) {
		DataSchema dataSchema;
		try {
			dataSchema = dataSchemaService.findById(dataSchemaId);
		} catch (ObjectNotFoundException e) {
			throw new PageValidationException(e);
		}
		model.addAttribute("dataSchema",dataSchema);
		List<Enumeration> dsList=enumerationService.list(1,Integer.MAX_VALUE);
		model.addAttribute("enumerationList",dsList);
		model.addAttribute("fieldTypes",FieldType.values());
		return "/schema/update";
	}
	
	@RequestMapping(method={RequestMethod.PUT},path={"/schema"})
	public String update(@RequestParam("dataSchemaId") Long dataSchemaId , @RequestParam("name") String name, @RequestParam("definition") String definition, Model model, HttpSession session) {
		DataSchema dbDataSchema;
		try {
			dbDataSchema = dataSchemaService.findById(dataSchemaId);
		} catch (ObjectNotFoundException e) {
			throw new PageValidationException(e);
		}
		dbDataSchema.setName(name);
		dbDataSchema.setDefinition(definition);
		try {
			dataSchemaService.update(dbDataSchema);
		} catch (ValidationException e) {
			throw new PageValidationException(e);
		}
		session.setAttribute(PAGE_MESSAGE_KEY,new PageMessage(getMessage(KEY_DATA_SCHEMA_UPDATE_SUCCEEDED),PageMessage.Type.info));
		return "redirect:/schema";
	}
	


	
}
