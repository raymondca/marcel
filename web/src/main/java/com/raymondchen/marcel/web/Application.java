package com.raymondchen.marcel.web;

import java.util.Arrays;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Main class to start the web application in Spring Context
 * @author Raymond
 *
 */
@SpringBootApplication(scanBasePackages={"com.raymondchen.marcel"})
@EnableJpaRepositories(basePackages="com.raymondchen.marcel")
public class Application {
private static Log logger = LogFactory.getLog(Application.class);

/**
 * Main method
 * @param args args
 */
    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(Application.class, args);

        logger.info("Let's inspect the beans provided by Spring Boot:");

        String[] beanNames = ctx.getBeanDefinitionNames();
        Arrays.sort(beanNames);
        for (String beanName : beanNames) {
        	logger.info(beanName);
        }
    }

}