package com.raymondchen.marcel.web.controller;

import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.raymondchen.marcel.model.exception.ValidationException;
import com.raymondchen.marcel.web.util.PageValidationException;


/**
 * Base class for all controller. Provide resource bundler matching and message lookup service
 * @author Raymond
 *
 */
public class BaseController {
	
	public static final String MESSAGES_KEY="messages";
	
	public static final String PAGE_MESSAGE_KEY="pageMessage";
	
	public static final String MODEL_KEY="model";
	
	private static Log logger = LogFactory.getLog(BaseController.class);
	
	public ResourceBundle getMessageResourceBundle() {
		Locale locale=Locale.getDefault();
		String resourceBaseName=getClass().getCanonicalName();
		logger.debug("resourceBaseName="+resourceBaseName);
		return ResourceBundle.getBundle(resourceBaseName, locale);
	}
	
	/**
	 * Get a message from the externalized resource bundle of the controller
	 * @param key
	 * @return
	 */
	public String getMessage(String key) {
		return getMessageResourceBundle().getString(key);
	}
	
	/**
	 * Get a message from the externalized resource bundle of the controller
	 * @param key
	 * @return
	 */
	public String getMessage(String key, String[] replacement) {
		String result=getMessageResourceBundle().getString(key);
		for (int i=1;i<=replacement.length;i++) {
			result=result.replaceAll("\\{"+i+"\\}", replacement[i-1]);
		}
		return result;
	}
	
	public void handleValidationException(ValidationException e) {
		e.setMessage(getMessage(e.getKey(), e.getParameters()));
		throw new PageValidationException(e);
	}
	
}
