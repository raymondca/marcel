package com.raymondchen.marcel.web.util;

public class PageMessage {
	
	public enum Type{
		info,warning,error
	}

	public PageMessage(String message, Type type) {
		this.setType(type);
		this.message = message;
	}

	private String message;
	private Type type;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}
}
