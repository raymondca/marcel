package com.raymondchen.marcel.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import com.raymondchen.marcel.model.exception.ValidationException;
import com.raymondchen.marcel.web.util.PageValidationException;

@ControllerAdvice
public class ValidationExceptionHandler {
	 public static final String DEFAULT_ERROR_VIEW = "/errorpage";

	  @ExceptionHandler(value = PageValidationException.class)
	  public ModelAndView
	  defaultErrorHandler(HttpServletRequest req, Exception e) throws Exception {
	    if (AnnotationUtils.findAnnotation
	                (e.getClass(), ResponseStatus.class) != null)
	      throw e;
	    // Otherwise setup and send the user to a default error-view.
	    ModelAndView mav = new ModelAndView();
	    mav.addObject("exception", e.getCause());
	    mav.addObject("message",e.getCause().getMessage());
	    mav.addObject("url", req.getRequestURL());
	    mav.setViewName(DEFAULT_ERROR_VIEW);
	    return mav;
	  }
}
