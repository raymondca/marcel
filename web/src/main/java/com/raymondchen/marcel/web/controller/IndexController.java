package com.raymondchen.marcel.web.controller;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.raymondchen.marcel.web.util.PageMessage;

/**
 * Main page controller
 * @author Raymond
 *
 */
@Controller
public class IndexController extends BaseController {

	/**
	 * URL of "/"
	 * @param name
	 * @param model
	 * @return the template name
	 */
	@RequestMapping({"/","/index"})
	public String index( Model model) {
		return "index";
	}
	
	@RequestMapping("/errorpage")
	public String errorPage(@RequestParam(value="name", required=false, defaultValue="Loser") String name, Model model) {
		model.addAttribute("name",name);
		return "errorpage";
	}
}
