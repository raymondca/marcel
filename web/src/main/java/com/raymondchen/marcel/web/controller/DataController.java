package com.raymondchen.marcel.web.controller;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.raymondchen.marcel.model.Data;
import com.raymondchen.marcel.model.DataSchema;
import com.raymondchen.marcel.model.Enumeration;
import com.raymondchen.marcel.model.dao.DataRepository;
import com.raymondchen.marcel.model.dao.DataSchemaRepository;
import com.raymondchen.marcel.model.dao.EnumerationRepository;
import com.raymondchen.marcel.model.exception.ObjectNotFoundException;
import com.raymondchen.marcel.model.exception.ValidationException;
import com.raymondchen.marcel.model.service.DataSchemaService;
import com.raymondchen.marcel.model.service.DataService;
import com.raymondchen.marcel.model.service.EnumerationService;
import com.raymondchen.marcel.web.util.PageMessage;
import com.raymondchen.marcel.web.util.PageValidationException;

/**
 * Data Admin controller
 * @author Raymond
 *
 */
@Controller
public class DataController extends BaseController {
	
	@Autowired
	private DataService dataService;
	
	@Autowired
	private DataSchemaService dataSchemaService;
	
	@Autowired
	private EnumerationService enumerationService;

	
	public static final String KEY_DATA_CREATION_SUCCEEDED="dataCreationSucceeded";
	public static final String KEY_DATA_CREATION_FAILED="dataCreationFailed";
	public static final String KEY_DATA_DELETION_SUCCEEDED="dataDeletionSucceeded";
	public static final String KEY_DATA_UPDATE_SUCCEEDED="dataUpdateSucceeded";

	/**
	 * URL of "/"
	 * @param name
	 * @param model
	 * @return the template name
	 */
	@RequestMapping(method={RequestMethod.GET},path={"/data/index","/data"})
	public String index( @RequestParam(value="page", required=false, defaultValue="1") int page, @RequestParam(value="pageSize", required=false, defaultValue="50") int pageSize, Model model) {
		model.addAttribute("page",page<0?0:page);
		model.addAttribute("pageSize",pageSize);
		Page<Data> dataset=dataService.findAllInPage(page, pageSize);
		model.addAttribute("dataset",dataset);
		return "/data/index";
	}
	
	@RequestMapping(path={"/data/create"})
	public String startCreate( Model model) {
		List<DataSchema> dsList=dataSchemaService.list(1,Integer.MAX_VALUE);
		model.addAttribute("dataSchemaList",dsList);
		List<Enumeration> enuLisst=enumerationService.list(1,Integer.MAX_VALUE);
		model.addAttribute("enumerationList",enuLisst);
		return "/data/create";
	}
	
	@RequestMapping(method={RequestMethod.POST},path={"/data"})
	public String create(@RequestParam("dataSchemaId") Long dataSchemaId ,@RequestParam("data") String data, Model model, HttpSession session) {
		Date now=new Date();
		Data newData=new Data();
		newData.setCreateTime(now);
		newData.setData(data);
		try {
			newData.setDataSchema(dataSchemaService.findById(dataSchemaId));
		} catch (ObjectNotFoundException e) {
			throw new PageValidationException(e);
		}
		newData.setLastModifiedTime(now);
		Data insertedData;
		try {
			insertedData = dataService.create(newData);
		} catch (ValidationException e) {
			throw new PageValidationException(e);
		}
		model.addAttribute(PAGE_MESSAGE_KEY,new PageMessage(getMessage(KEY_DATA_CREATION_SUCCEEDED),PageMessage.Type.info));
		if (insertedData.getId()!=null) {
			session.setAttribute(PAGE_MESSAGE_KEY,new PageMessage(getMessage(KEY_DATA_CREATION_SUCCEEDED),PageMessage.Type.info));
		} else {
			session.setAttribute(PAGE_MESSAGE_KEY,new PageMessage(KEY_DATA_CREATION_FAILED,PageMessage.Type.error));
		}
		return "redirect:/data/index";
	}
	
	@RequestMapping(method={RequestMethod.DELETE},path={"/data"})
	public String delete(@RequestParam("dataId") Long dataId , Model model, HttpSession session) {
		dataService.deleteById(dataId);
		session.setAttribute(PAGE_MESSAGE_KEY,new PageMessage(getMessage(KEY_DATA_DELETION_SUCCEEDED),PageMessage.Type.info));
		return "redirect:/data/index";
	}
	
	@RequestMapping(method={RequestMethod.GET},path={"/data/update"})
	public String startUpdate(@RequestParam("dataId") Long dataId , Model model) {
		Data data;
		try {
			data = dataService.findById(dataId);
		} catch (ObjectNotFoundException e) {
			throw new PageValidationException(e);
		}
		model.addAttribute("data",data);
		List<DataSchema> dsList=dataSchemaService.list(1,Integer.MAX_VALUE);
		model.addAttribute("dataSchemaList",dsList);
		List<Enumeration> enuLisst=enumerationService.list(1,Integer.MAX_VALUE);
		model.addAttribute("enumerationList",enuLisst);
		return "/data/update";
	}
	
	@RequestMapping(method={RequestMethod.PUT},path={"/data"})
	public String update(@RequestParam("dataId") Long dataId , @RequestParam("data") String data, Model model, HttpSession session) {
		Data dbData;
		try {
			dbData = dataService.findById(dataId);
		} catch (ObjectNotFoundException e) {
			throw new PageValidationException(e);
		}
		dbData.setData(data);
		try {
			dataService.update(dbData);
		} catch (ValidationException e) {
			throw new PageValidationException(e);
		}
		session.setAttribute(PAGE_MESSAGE_KEY,new PageMessage(getMessage(KEY_DATA_DELETION_SUCCEEDED),PageMessage.Type.info));
		return "redirect:/data";
	}
}
