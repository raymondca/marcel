package com.raymondchen.marcel.web.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.raymondchen.marcel.model.Enumeration;
import com.raymondchen.marcel.model.exception.ObjectNotFoundException;
import com.raymondchen.marcel.model.exception.SystemException;
import com.raymondchen.marcel.model.exception.ValidationException;
import com.raymondchen.marcel.model.service.EnumerationService;
import com.raymondchen.marcel.web.util.PageMessage;


/**
 * Enumeration Admin controller
 * @author Raymond
 *
 */
@Controller
public class EnumController extends BaseController  {

	public static final String KEY_ENUMERATION_CREATION_SUCCEEDED="enumCreationSucceeded";
	public static final String KEY_ENUMERATION_CREATION_FAILED="enumCreationFailed";
	public static final String KEY_ENUMERATION_DELETION_SUCCEEDED="enumDeletionSucceeded";
	public static final String KEY_ENUMERATION_UPDATE_SUCCEEDED="enumUpdateSucceeded";
	public static final String KEY_ERROR_ENUMERATION_NAME_EXIST="ErrorEnumerationNameExist";
	public static final String KEY_ERROR_ENUMERATION_HAS_DUPLICATE="ErrorEnumerationHasDuplicate";
	
	@Autowired
	private EnumerationService enumerationService;
	/**
	 * URL of "/"
	 * @param name
	 * @param model
	 * @return the template name
	 */
	@RequestMapping(method={RequestMethod.GET},path={"/enum/index","/enum"})
	public String index( @RequestParam(value="page", required=false, defaultValue="1") int page, @RequestParam(value="pageSize", 
		required=false, defaultValue="50") int pageSize, Model model) {
		model.addAttribute("page",page<0?0:page);
		model.addAttribute("pageSize",pageSize);
		Page<Enumeration> dataset=enumerationService.findAllInPage(page,pageSize);
		model.addAttribute("dataset",dataset);
		return "/enum/index";
	}
	
	@RequestMapping(path={"/enum/create"})
	public String startCreate( Model model) {
		return "/enum/create";
	}
	
	@RequestMapping(method={RequestMethod.POST},path={"/enum"})
	public String createData(@RequestParam("name") String name ,@RequestParam("value") String value, Model model, HttpSession session) {
		Enumeration newEnum=new Enumeration();
		newEnum.setName(name);
		newEnum.setValue(value);
		Enumeration insertedEnum=null;
		try {
			insertedEnum = enumerationService.create(newEnum);
		} catch (ValidationException e) {
			handleValidationException(e);
		}
		model.addAttribute(PAGE_MESSAGE_KEY,new PageMessage(getMessage(KEY_ENUMERATION_CREATION_SUCCEEDED),PageMessage.Type.info));
		if (insertedEnum.getId()!=null) {
			session.setAttribute(PAGE_MESSAGE_KEY,new PageMessage(getMessage(KEY_ENUMERATION_CREATION_SUCCEEDED),PageMessage.Type.info));
		} else {
			session.setAttribute(PAGE_MESSAGE_KEY,new PageMessage(KEY_ENUMERATION_CREATION_FAILED,PageMessage.Type.error));
		}
		return "redirect:/enum/index";
	}
	
	@RequestMapping(method={RequestMethod.DELETE},path={"/enum"}) 
	public String delete(@RequestParam("enumId") Long enumId , Model model, HttpSession session) {
		enumerationService.deleteById(enumId);
		session.setAttribute(PAGE_MESSAGE_KEY,new PageMessage(getMessage(KEY_ENUMERATION_DELETION_SUCCEEDED),PageMessage.Type.info));
		return "redirect:/enum/index";
	}
	
	@RequestMapping(method={RequestMethod.GET},path={"/enum/update"}) 
	public String startUpdate(@RequestParam("enumId") Long enumId , Model model) {
		Enumeration enumeration;
		try {
			enumeration = enumerationService.findById(enumId);
		} catch (ObjectNotFoundException e) {
			throw new SystemException(e);
		}
		model.addAttribute("enumeration",enumeration);
		return "/enum/update";
	}
	
	@RequestMapping(method={RequestMethod.PUT},path={"/enum"})
	public String update(@RequestParam("enumId") Long enumId , @RequestParam("name") String name, @RequestParam("value") String value, Model model, HttpSession session) {
		Enumeration dbEnum;
		try {
			dbEnum = enumerationService.findById(enumId);
		} catch (ObjectNotFoundException e) {
			throw new SystemException(e);
		}
		dbEnum.setName(name);
		dbEnum.setValue(value);
		try {
			enumerationService.update(dbEnum);
		} catch (ValidationException e) {
			handleValidationException(e);
		}
		session.setAttribute(PAGE_MESSAGE_KEY,new PageMessage(getMessage(KEY_ENUMERATION_UPDATE_SUCCEEDED),PageMessage.Type.info));
		return "redirect:/enum";
	}
	
}
