package com.raymondchen.marcel.web.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.raymondchen.marcel.web.controller.BaseController;

@Component
public class ControllerInterceptor extends HandlerInterceptorAdapter {

	/**
	 * Add MessageSource to the model map as key 'messages'
	 * Add controller object (this) to the model
	 */
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		if (handler instanceof HandlerMethod) {
			HandlerMethod method = (HandlerMethod) handler;
			if (method.getBean() instanceof BaseController) {
				BaseController controller = (BaseController) method.getBean();
				modelAndView.getModel().put(BaseController.MODEL_KEY, this);
				modelAndView.getModel().put(BaseController.MESSAGES_KEY, controller.getMessageResourceBundle());
			}
		}
	}

}
