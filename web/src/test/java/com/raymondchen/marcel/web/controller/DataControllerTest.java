package com.raymondchen.marcel.web.controller;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.ui.ExtendedModelMap;

import com.raymondchen.marcel.model.Data;
import com.raymondchen.marcel.test.BaseTestCase;
import com.raymondchen.marcel.web.Application;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class DataControllerTest extends BaseTestCase {

	@Autowired
	private DataController dataController;

	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void index() {
		int page=1;
		int pageSize=10;
		ExtendedModelMap map=new ExtendedModelMap();
		String index=dataController.index(page,pageSize, map);
		Assert.assertEquals("/data/index", index);
		@SuppressWarnings("unchecked")
		Page<Data> dataset=(Page<Data>)map.get("dataset");
		Assert.assertNotNull(dataset);
		Assert.assertTrue(dataset.getTotalElements()>=1);
		List<Data> list=dataset.getContent();
		System.out.println(list.get(0).getData());
	}
	
	@Test
	public void getErrorpage() {

	}

}