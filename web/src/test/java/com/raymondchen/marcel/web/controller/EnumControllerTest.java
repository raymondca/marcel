package com.raymondchen.marcel.web.controller;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;

import com.raymondchen.marcel.model.Enumeration;
import com.raymondchen.marcel.model.dao.EnumerationRepository;
import com.raymondchen.marcel.model.exception.SystemException;
import com.raymondchen.marcel.model.exception.ValidationException;
import com.raymondchen.marcel.model.validation.ModelValidator;
import com.raymondchen.marcel.test.BaseTestCase;
import com.raymondchen.marcel.web.Application;
import com.raymondchen.marcel.web.util.PageValidationException;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class EnumControllerTest extends BaseTestCase {

	@Autowired
	private EnumController enumController;
	
	@Autowired
	private EnumerationRepository enumerationRepository;
	
	int page=1;
	int pageSize=10;
	
	@Test
	public void testIndex() {
		Model model=new ExtendedModelMap();
		String returnPage=enumController.index( page, pageSize,  model);
		Assert.assertEquals("/enum/index", returnPage);
	}

	@Test
	public void testStartCreate() {
		Model model=new ExtendedModelMap();
		String returnPage=enumController.startCreate(model);
		Assert.assertEquals("/enum/create", returnPage);
	}

	@Test
	public void testCreateData() {
		Model model=new ExtendedModelMap();
		String name="name";
		String value="value";
		MockHttpSession httpSession=new MockHttpSession();
		try {
			enumController.createData(name,value,model,httpSession);
			Assert.fail("Should throw an exception");
		} catch (PageValidationException e) {
			if (e.getCause() instanceof ValidationException) {
				Assert.assertEquals(ModelValidator.KEY_ERROR_JSON_FORMAT_ERROR,((ValidationException)e.getCause()).getKey());
			}
		}
		value="[\"abcd\"]";
		String returnPage=enumController.createData(name, value, model, httpSession);
		Assert.assertEquals("redirect:/enum/index", returnPage);
	}

	@Test
	public void testDelete() {
		Model model=new ExtendedModelMap();
		Long enumId=this.enumerationId;
		MockHttpSession httpSession=new MockHttpSession();
		String returnPage=enumController.delete(enumId,model,httpSession);
		Assert.assertEquals("redirect:/enum/index", returnPage);
		Enumeration enum2=enumerationRepository.findOne(enumId);
		Assert.assertNull(enum2);
	}

	@Test
	public void testStart() {
		ExtendedModelMap model=new ExtendedModelMap();
		Long enumId=super.enumerationId;
		String returnPage=enumController.startUpdate(enumId,model);
		Assert.assertEquals("/enum/update", returnPage);
		Assert.assertNotNull(model.get("enumeration"));
		enumId=-1L; // NotExisting
		model=new ExtendedModelMap();
		try {
			returnPage=enumController.startUpdate(enumId,model);
			Assert.fail("There should be an exception");
		} catch (SystemException e) {
			Assert.assertTrue("non-existing ID should bring us to exception",true);
		}
		
	}

	@Test(expected=PageValidationException.class)
	public void testUpdateWithMalformedJsonValue() {
		Model model=new ExtendedModelMap();
		Long enumId=super.enumerationId;
		String name="name";
		String value="value";
		MockHttpSession httpSession=new MockHttpSession();
		enumController.update(enumId,name,value,model,httpSession);
	}
	
	@Test()
	public void testUpdate() {
		Model model=new ExtendedModelMap();
		Long enumId=super.enumerationId;
		String name="UniqueName"+System.currentTimeMillis();
		String value="[\"Red\",\"Green\"]";
		MockHttpSession httpSession=new MockHttpSession();
		String returnPage=enumController.update(enumId,name,value,model,httpSession);
		Assert.assertEquals("redirect:/enum", returnPage);
		Assert.assertNotNull(httpSession.getAttribute("pageMessage"));
		Enumeration enum2=enumerationRepository.findOne(enumId);
		Assert.assertEquals(name, enum2.getName());
		Assert.assertEquals(value, enum2.getValue());
	}

}
