package com.raymondchen.marcel.web.controller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.ui.ExtendedModelMap;

import com.raymondchen.marcel.web.Application;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class IndexControllerTest {

	@Autowired
	private IndexController indexController;

	@Before
	public void setUp() {
	}

	@Test
	public void index() {
		ExtendedModelMap map=new ExtendedModelMap();
		String index=indexController.index(map);
		Assert.assertEquals("index", index);
	}
	
	@Test
	public void getErrorpage() {

	}

}