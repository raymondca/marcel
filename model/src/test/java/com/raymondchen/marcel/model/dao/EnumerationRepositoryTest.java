package com.raymondchen.marcel.model.dao;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.raymondchen.marcel.Application;
import com.raymondchen.marcel.model.Enumeration;
import com.raymondchen.marcel.test.BaseTestCase;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class EnumerationRepositoryTest extends BaseTestCase {

	@Autowired
	private EnumerationRepository enumerationRepository;

	@Test
	public void testFindOne() {
		Enumeration en = enumerationRepository.findOne(this.enumerationId);
		Assert.assertNotNull(en);
		Assert.assertEquals("Should contain only one record", this.enumerationName, en.getName());
	}

	@Test
	public void testDelete() {
		enumerationRepository.delete(this.enumerationId);
		Assert.assertNull(enumerationRepository.findOne(this.enumerationId));
	}
}
