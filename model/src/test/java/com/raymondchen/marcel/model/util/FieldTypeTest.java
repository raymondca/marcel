package com.raymondchen.marcel.model.util;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

public class FieldTypeTest {

	@Test
	public void testIsBuiltIn() {
		Assert.assertTrue(FieldType.String.isBuiltIn());
		Assert.assertTrue(FieldType.Integer.isBuiltIn());
		Assert.assertTrue(FieldType.Double.isBuiltIn());
		Assert.assertFalse(FieldType.Enumeration.isBuiltIn());
	}

}
