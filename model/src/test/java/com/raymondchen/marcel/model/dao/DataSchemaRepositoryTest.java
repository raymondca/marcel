package com.raymondchen.marcel.model.dao;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.raymondchen.marcel.Application;
import com.raymondchen.marcel.model.DataSchema;
import com.raymondchen.marcel.model.dao.DataSchemaRepository;
import com.raymondchen.marcel.test.BaseTestCase;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class DataSchemaRepositoryTest extends BaseTestCase {
	@Autowired
	private DataSchemaRepository dataSchemaRepository;
	
	@Test
	public void testFindOne() {
	  DataSchema ds=	dataSchemaRepository.findOne(this.dataSchemaId);
	  Assert.assertNotNull(ds);
	  Assert.assertEquals("Should contain only one record", dataSchemaId ,ds.getId());
	}
	
	@Test
	public void testDelete() {
		dataSchemaRepository.delete(dataSchemaId);
	  Assert.assertNull(dataSchemaRepository.findOne(dataSchemaId));
	}

}
