package com.raymondchen.marcel.model.service;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.raymondchen.marcel.Application;
import com.raymondchen.marcel.model.Data;
import com.raymondchen.marcel.model.DataSchema;
import com.raymondchen.marcel.model.exception.ObjectNotFoundException;
import com.raymondchen.marcel.model.exception.ValidationException;
import com.raymondchen.marcel.test.BaseTestCase;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class DataServiceTest extends BaseTestCase {

	@Autowired
	private DataService dataService;
	
	@Autowired
	private DataSchemaService dataSchemaService;
	
	@Test
	public void testFindById() throws ObjectNotFoundException {
		Data d=dataService.findById(super.dataId);
		Assert.assertNotNull(d);
	}

	@Test(expected=ObjectNotFoundException.class)
	public void testDeleteById() throws ObjectNotFoundException {
		dataService.deleteById(super.dataId);
		dataService.findById(super.dataId);
	}

	@Test
	public void testList() {
		List<Data> list=dataService.list(1, 99);
		Assert.assertFalse(list.isEmpty());
	}

	@Test
	public void testCreate() throws ObjectNotFoundException, ValidationException {
		DataSchema ds=dataSchemaService.findById(super.dataSchemaId);
		Data d=new Data();
		d.setDataSchema(ds);
		d.setData("[{\"name\":\"CarModel\",\"value\":\"Toyota\"},{\"name\":\"Color\",\"value\":\"Red\"}]");
		Data dNew=dataService.create(d);
		Assert.assertNotNull(dNew);
	}

	@Test
	public void testUpdate() throws ObjectNotFoundException, ValidationException {
		Data d=dataService.findById(super.dataId);
		d.setData("[{\"name\":\"CarModel\",\"value\":\"Toyota\"},{\"name\":\"Color\",\"value\":\"Red\"}]");
		Data dNew=dataService.update(d);
		Data d2=dataService.findById(super.dataId);
		Assert.assertEquals("[{\"name\":\"CarModel\",\"value\":\"Toyota\"},{\"name\":\"Color\",\"value\":\"Red\"}]",d2.getData());
	}

	@Test
	public void testFindByDataSchemaId() {
		List<Data> list=dataService.findByDataSchemaId(super.dataSchemaId);
		Assert.assertFalse(list.isEmpty());
	}

	@Test
	public void testFindAllInPage() {
		Page<Data> page=dataService.findAllInPage(1, 88);
		Assert.assertEquals(0,page.getNumber());
		Assert.assertEquals(1,page.getTotalPages());
		Assert.assertFalse(page.getContent().isEmpty());
	}

}
