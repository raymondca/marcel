package com.raymondchen.marcel.model.impl;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.raymondchen.marcel.Application;
import com.raymondchen.marcel.model.Data;
import com.raymondchen.marcel.model.dao.DataRepositoryCustom;
import com.raymondchen.marcel.test.BaseTestCase;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class DataRepositoryCustomImplTest extends BaseTestCase {
	
	@Autowired
	private DataRepositoryCustom dataRepositoryCustom;

	@Test
	public void testFindByDataSchemaId() {
		List<Data> list=dataRepositoryCustom.findByDataSchemaId(super.dataSchemaId);
		Assert.assertTrue(!list.isEmpty());
	}

}
