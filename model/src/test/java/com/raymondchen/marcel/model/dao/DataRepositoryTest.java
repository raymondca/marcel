package com.raymondchen.marcel.model.dao;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.raymondchen.marcel.Application;
import com.raymondchen.marcel.model.Data;
import com.raymondchen.marcel.test.BaseTestCase;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class DataRepositoryTest extends BaseTestCase {

	@Autowired
	private DataRepository dataRepository;
	
	@Test
	public void testFindOneID() {
		Data data=dataRepository.findOne(super.dataId);
		Assert.assertEquals(super.data, data.getData());
	}

	@Test
	public void testDeleteID() {
		dataRepository.delete(super.dataId);
		Data data=dataRepository.findOne(super.dataId);
		Assert.assertNull(data);
	}

}
