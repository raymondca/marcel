package com.raymondchen.marcel.model.service;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.raymondchen.marcel.Application;
import com.raymondchen.marcel.model.Enumeration;
import com.raymondchen.marcel.model.exception.ObjectNotFoundException;
import com.raymondchen.marcel.model.exception.ValidationException;
import com.raymondchen.marcel.test.BaseTestCase;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class EnumerationServiceTest extends BaseTestCase {
	
	@Autowired
	private EnumerationService enumerationService;

	@Test
	public void testFindById() throws ObjectNotFoundException {
		Enumeration enu=enumerationService.findById(super.enumerationId);
		Assert.assertNotNull(enu);
	}

	@Test(expected=ObjectNotFoundException.class)
	public void testDeleteById() throws ObjectNotFoundException {
		enumerationService.deleteById(super.enumerationId);
		enumerationService.findById(super.enumerationId);
	}

	@Test
	public void testList() {
		List<Enumeration> list=enumerationService.list(1, 99);
		Assert.assertFalse(list.isEmpty());
	}

	@Test
	public void testCreate() throws ValidationException {
		Enumeration enu=new Enumeration();
		enu.setName("UnitTestNewEnuName");
		enu.setValue("[\"A\",\"B\",\"C\",\"D\"]");
		enu=enumerationService.create(enu);
		Assert.assertNotNull(enu.getId());
	}

	@Test
	public void testUpdate() throws ObjectNotFoundException, ValidationException {
		Enumeration enu=enumerationService.findById(super.enumerationId);
		enu.setValue("[\"A\",\"B\",\"C\",\"D\"]");
		enu=enumerationService.update(enu);
		enu=enumerationService.findById(super.enumerationId);
		Assert.assertEquals("[\"A\",\"B\",\"C\",\"D\"]",enu.getValue());
	}

	@Test
	public void testFindByEnumerationName() {
		List<Enumeration> list=enumerationService.findByEnumerationName(super.enumerationName);
		Assert.assertFalse(list.isEmpty());
	}

	@Test
	public void testFindAllInPage() {
		Page<Enumeration> page=enumerationService.findAllInPage(1, 99);
		Assert.assertEquals(0,page.getNumber());
		Assert.assertEquals(1,page.getTotalPages());
		Assert.assertFalse(page.getContent().isEmpty());
	}

}
