package com.raymondchen.marcel.model.service;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.raymondchen.marcel.Application;
import com.raymondchen.marcel.model.DataSchema;
import com.raymondchen.marcel.model.exception.ObjectNotFoundException;
import com.raymondchen.marcel.model.exception.ValidationException;
import com.raymondchen.marcel.test.BaseTestCase;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class DataSchemaServiceTest extends BaseTestCase {
	
	@Autowired
	private DataSchemaService dataSchemaService;
	
	@Test
	public void testFindById() throws ObjectNotFoundException   {
		DataSchema ds=dataSchemaService.findById(super.dataSchemaId);
		Assert.assertNotNull(ds);
	}

	@Test(expected=ObjectNotFoundException.class)
	public void testDeleteById() throws ObjectNotFoundException {
		dataSchemaService.deleteById(super.dataSchemaId);
		DataSchema ds=dataSchemaService.findById(super.dataSchemaId);
		Assert.assertNull(ds);
	}

	@Test
	public void testList() {
		List<DataSchema> list=dataSchemaService.list(1, 10);
		Assert.assertFalse(list.isEmpty());
	}

	@Test
	public void testCreate() throws ObjectNotFoundException, ValidationException {
		DataSchema ds=new DataSchema();
		ds.setName("UnitTestNewName1");
		ds.setVersion(1L);
		ds.setDefinition("[{\"name\":\"CarModel\",\"value\":\"Raymond\"}]");
		DataSchema newDs=dataSchemaService.create(ds);
		Assert.assertNotNull(newDs.getId());
	}

	@Test
	public void testUpdate() throws ObjectNotFoundException, ValidationException {
		DataSchema ds=dataSchemaService.findById(super.dataSchemaId);
		ds.setDefinition("[{\"name\":\"CarModel\",\"value\":\"Raymond\"}]");
		dataSchemaService.update(ds);
		DataSchema dsNew=dataSchemaService.findById(super.dataSchemaId);
		Assert.assertEquals("[{\"name\":\"CarModel\",\"value\":\"Raymond\"}]",dsNew.getDefinition());
	}

	@Test
	public void testFindBySchemaName() {
		List<DataSchema> list=dataSchemaService.findBySchemaName(super.dataSchemaName);
		Assert.assertFalse(list.isEmpty());
	}

	@Test
	public void testFindAllInPage() {
		Page<DataSchema> page=dataSchemaService.findAllInPage(1, 99);
		Assert.assertEquals(0, page.getNumber());
		Assert.assertEquals(1, page.getTotalPages());
		Assert.assertFalse(page.getContent().isEmpty());
	}

}
