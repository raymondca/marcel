package com.raymondchen.marcel.model.validation;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.raymondchen.marcel.Application;
import com.raymondchen.marcel.model.DataSchema;
import com.raymondchen.marcel.model.Enumeration;
import com.raymondchen.marcel.model.dao.EnumerationRepository;
import com.raymondchen.marcel.model.exception.ValidationException;
import com.raymondchen.marcel.test.BaseTestCase;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class ModelValidatorTest  extends BaseTestCase {
	
	@Autowired
	private ModelValidator modelValidator;
	
	@Autowired
	private EnumerationRepository enumerationRepository;

	@Test
	public void testValidateNewEnum() {
		// Case value is not well-formed json
		Enumeration enu=new Enumeration();
		enu.setValue("not a json");
		enu.setName("test1");
		try {
			modelValidator.validateNewEnum(enu);
			Assert.fail("Shouldn't reach here");
		} catch (ValidationException e) {
			Assert.assertEquals(ModelValidator.KEY_ERROR_JSON_FORMAT_ERROR,e.getKey());
		}
		
		// Case name is empty
		enu.setName("");
		try {
			modelValidator.validateNewEnum(enu);
			Assert.fail("Shouldn't reach here");
		} catch (ValidationException e) {
			Assert.assertEquals(ModelValidator.KEY_ERROR_ENUMERATION_NAME_OR_VALUE_EMPTY,e.getKey());
		}
		enu.setName("NonEMpty");
		enu.setValue("");
		try {
			modelValidator.validateNewEnum(enu);
			Assert.fail("Shouldn't reach here");
		} catch (ValidationException e) {
			Assert.assertEquals(ModelValidator.KEY_ERROR_ENUMERATION_NAME_OR_VALUE_EMPTY,e.getKey());
		}
		enu.setName(super.enumerationName); //Color already exists
		enu.setValue("[\"Mangenta\"]");
		try {
			modelValidator.validateNewEnum(enu);
			Assert.fail("Shouldn't reach here");
		} catch (ValidationException e) {
			Assert.assertEquals(ModelValidator.KEY_ERROR_ENUMERATION_NAME_EXIST,e.getKey());
		}
		enu.setName("UniformColor");
		enu.setValue("[\"Mangenta\",\"Violet\",\"Sapphire\",\"Mangenta\"]");
		try {
			modelValidator.validateNewEnum(enu);
			Assert.fail("Shouldn't reach here");
		} catch (ValidationException e) {
			Assert.assertEquals(ModelValidator.KEY_ERROR_ENUMERATION_HAS_DUPLICATE,e.getKey());
		}
	}
	
	@Test
	public void testValidateUpdateEnum() {
		// Case value is not well-formed json
		Enumeration enu=enumerationRepository.findOne(enumerationId);
		enu.setValue("not a json");
		enu.setName("test1");
		try {
			modelValidator.validateUpdateEnum(enu);
			Assert.fail("Shouldn't reach here");
		} catch (ValidationException e) {
			Assert.assertEquals(ModelValidator.KEY_ERROR_JSON_FORMAT_ERROR,e.getKey());
		}
		
		// Case name is empty
		enu.setName("");
		try {
			modelValidator.validateNewEnum(enu);
			Assert.fail("Shouldn't reach here");
		} catch (ValidationException e) {
			Assert.assertEquals(ModelValidator.KEY_ERROR_ENUMERATION_NAME_OR_VALUE_EMPTY,e.getKey());
		}
		enu.setName("NonEMpty");
		enu.setValue("");
		try {
			modelValidator.validateNewEnum(enu);
			Assert.fail("Shouldn't reach here");
		} catch (ValidationException e) {
			Assert.assertEquals(ModelValidator.KEY_ERROR_ENUMERATION_NAME_OR_VALUE_EMPTY,e.getKey());
		}
		enu.setName("Color"); //Color already exists
		enu.setValue("[\"Mangenta\"]");
		try {
			modelValidator.validateNewEnum(enu);
			Assert.fail("Shouldn't reach here");
		} catch (ValidationException e) {
			Assert.assertEquals(ModelValidator.KEY_ERROR_ENUMERATION_NAME_EXIST,e.getKey());
		}
		enu.setName("UniformColor");
		enu.setValue("[\"Mangenta\",\"Violet\",\"Sapphire\",\"Mangenta\"]");
		try {
			modelValidator.validateNewEnum(enu);
			Assert.fail("Shouldn't reach here");
		} catch (ValidationException e) {
			Assert.assertEquals(ModelValidator.KEY_ERROR_ENUMERATION_HAS_DUPLICATE,e.getKey());
		}
	}
	
	@Test
	public void testValidateNewDataSchema()  throws ValidationException{
		String schemaName="UnitTestSchemaName";
		String validDefinition = "[{\"name\":\"Name\",\"fieldType\":1},{\"name\":\"Make\",\"fieldType\":1},{\"name\":\"Year\",\"fieldType\":2},{\"name\":\"Color\",\"fieldType\":4,\"enumerationId\":1}]";
		DataSchema ds=new DataSchema();
		ds.setName(schemaName);
		ds.setDefinition(validDefinition);
		modelValidator.validateNewDataSchema(ds);
		Assert.assertTrue("No exception should be thrown",true);
	}
	
	@Test(expected=ValidationException.class)
	public void testValidateNewDataSchemaWithEmptySchemaName()  throws ValidationException{
		String validDefinition = "[{\"name\":\"Name\",\"fieldType\":1},{\"name\":\"Make\",\"fieldType\":1},{\"name\":\"Year\",\"fieldType\":2},{\"name\":\"Color\",\"fieldType\":4,\"enumerationId\":1}]";
		DataSchema ds=new DataSchema();
		ds.setName("");
		ds.setDefinition(validDefinition);
		modelValidator.validateNewDataSchema(ds);
		Assert.assertFalse("ValidateException should be thrown",true);
	}
	
	@Test(expected=ValidationException.class)
	public void testValidateNewDataSchemaWithEmptyDefinition()  throws ValidationException{
		String schemaName="UnitTestSchemaName";
		DataSchema ds=new DataSchema();
		ds.setName(schemaName);
		ds.setDefinition("");
		modelValidator.validateNewDataSchema(ds);
		Assert.assertFalse("ValidateException should be thrown",true);
	}
	
	@Test(expected=ValidationException.class)
	public void testValidateNewDataSchemaWithEmptyDefinitionJSONArray()  throws ValidationException{
		String schemaName="UnitTestSchemaName";
		DataSchema ds=new DataSchema();
		ds.setName(schemaName);
		ds.setDefinition("[]");
		modelValidator.validateNewDataSchema(ds);
		Assert.assertFalse("ValidateException should be thrown",true);
	}
	
	@Test(expected=ValidationException.class)
	public void testValidateNewDataSchemaWithDuplicateFieldNamesInDefinitionJSONArray()  throws ValidationException{
		String schemaName="UnitTestSchemaName";
		String validDefinition = "[{\"name\":\"Name\",\"fieldType\":1},{\"name\":\"Make\",\"fieldType\":1},{\"name\":\"Year\",\"fieldType\":2},{\"name\":\"Make\",\"fieldType\":4,\"enumerationId\":1}]";
		DataSchema ds=new DataSchema();
		ds.setName(schemaName);
		ds.setDefinition(validDefinition);
		modelValidator.validateNewDataSchema(ds);
		Assert.assertFalse("ValidateException should be thrown",true);
	}
	
	@Test(expected=ValidationException.class)
	public void testValidateNewDataSchemaWithDuplicateSchemaName() throws ValidationException {
		String validDefinition = "[{\"name\":\"Name\",\"fieldType\":1},{\"name\":\"Make\",\"fieldType\":1},{\"name\":\"Year\",\"fieldType\":2},{\"name\":\"Color\",\"fieldType\":4,\"enumerationId\":1}]";
		DataSchema ds=new DataSchema();
		ds.setName(super.dataSchemaName);
		ds.setDefinition(validDefinition);
		modelValidator.validateNewDataSchema(ds);
		Assert.assertFalse("ValidateException should be thrown",true);
	}

}
