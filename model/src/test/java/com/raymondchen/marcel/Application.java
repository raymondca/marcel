package com.raymondchen.marcel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@Configuration
@ComponentScan
@EnableAutoConfiguration
public class Application {
	public Application() {
		//Default constructor is required for cglib
	}
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
