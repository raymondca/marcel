package com.raymondchen.marcel.test;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.raymondchen.marcel.Application;
import com.raymondchen.marcel.model.Data;
import com.raymondchen.marcel.model.DataSchema;
import com.raymondchen.marcel.model.Enumeration;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT, classes = Application.class)
@WebAppConfiguration
@Transactional
public class BaseTestCase {
	private Log logger = LogFactory.getLog(BaseTestCase.class);
	
	@PersistenceContext
	EntityManager em;
	
	protected Long dataId;
	protected Long dataSchemaId;
	protected Long enumerationId;
	
	protected String enumerationName="UnitTestColor";
	protected String dataSchemaName="UnitTestBaseDataSchemaName";
	protected String dataSchemaDefinition="[{\"name\":\"CarModel\",\"fieldType\":1},{\"name\":\"Color\",\"fieldType\":4,\"enumerationId\":12}]";
	protected String data="[{\"name\":\"CarModel\",\"value\":\"Toyota\"},{\"name\":\"Color\",\"value\":\"Red\"},{\"name\":\"Happy\",\"value\":2323}]";
	
	@Before public void setUp() {
		logger.debug("Setting up JUnit test case...");
		Enumeration enu=new Enumeration();
		 enu.setName(enumerationName);
		 enu.setValue("[\"Red\",\"Orange\",\"Yellow\",\"Green\",\"Blue\",\"Indigo\",\"Violet\"]");
		 DataSchema ds=new DataSchema();
		 ds.setDefinition(dataSchemaDefinition);
		 ds.setName(dataSchemaName);
		 Data d=new Data();
		 d.setCreateTime(new Date());
		 d.setDataSchema(ds);
		 d.setLastModifiedTime(new Date());
		 d.setData(data);
		 em.persist(ds);
		 em.persist(d);
		 em.persist(enu);
		 this.dataId=d.getId();
		 this.dataSchemaId=ds.getId();
		 this.enumerationId=enu.getId();
		 logger.debug("Sample data created. dataId="+dataId+" dataSchemaId="+dataSchemaId+" enumerationId="+enumerationId);
	}
	
	@After public void tearDown() {
		logger.debug("Tearing down JUnit test case...");
	}
	
	/**
	 * Make sure the testing data are correctly inserted
	 */
	@Test
	public void testBasic() {
		String sql = "select * from enumeration where name like '"+this.enumerationName+"%'";
		Query query = em.createNativeQuery(sql);
		logger.info("Result size=" + query.getResultList().size());
		Assert.assertTrue("", query.getResultList().size() > 0);
	}
}
