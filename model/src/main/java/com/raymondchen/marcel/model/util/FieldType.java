package com.raymondchen.marcel.model.util;

public enum FieldType {
	String,Integer,Double,Enumeration;

	public boolean isBuiltIn() {
		return !this.equals(Enumeration);
	}
}
