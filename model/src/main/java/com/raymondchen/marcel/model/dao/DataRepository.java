package com.raymondchen.marcel.model.dao;


import org.springframework.data.jpa.repository.JpaRepository;

import com.raymondchen.marcel.model.Data;

public interface DataRepository extends JpaRepository<Data, Long> {
}
