package com.raymondchen.marcel.model.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.raymondchen.marcel.model.Enumeration;

/**
 * Repository to access Enumeration data
 * @author Raymond
 *
 */
public interface EnumerationRepository extends JpaRepository<Enumeration, Long> {

}
