package com.raymondchen.marcel.model.exception;

public class ValidationException extends Exception {

	private final String key;
	
	private final String[] parameters;
	
	private String message;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ValidationException(String key, String[] parameters) {
		this(key,parameters,null);
	}
	
	public ValidationException(String key, String[] parameters, String message) {
		this.key=key;
		this.parameters=parameters;
		this.message=message;
	}

	public String getKey() {
		return key;
	}



	public String[] getParameters() {
		return parameters;
	}

	
	
	@Override
	public String getMessage() {
		if (this.message!=null) {
			return this.message;
		} else {
			return super.getMessage();
		}
	}
	
	public void setMessage(String message) {
		this.message=message;
	}


}
