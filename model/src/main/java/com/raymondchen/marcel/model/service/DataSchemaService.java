package com.raymondchen.marcel.model.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.raymondchen.marcel.model.DataSchema;
import com.raymondchen.marcel.model.exception.ObjectNotFoundException;
import com.raymondchen.marcel.model.exception.ValidationException;

public interface DataSchemaService {
	DataSchema findById(Long id) throws ObjectNotFoundException;
	void deleteById(Long id);
	List<DataSchema> list(int page, int pageSize) ;
	DataSchema create(DataSchema dataSchema)throws ValidationException;
	DataSchema update(DataSchema dataSchema)throws ValidationException;
	List<DataSchema> findBySchemaName(String schemaName);
	Page<DataSchema> findAllInPage(int page, int pageSize);
}
