package com.raymondchen.marcel.model.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.raymondchen.marcel.model.Enumeration;
import com.raymondchen.marcel.model.exception.ObjectNotFoundException;
import com.raymondchen.marcel.model.exception.ValidationException;

public interface EnumerationService {
	Enumeration findById(Long id) throws ObjectNotFoundException;
	void deleteById(Long id);
	List<Enumeration> list(int page, int pageSize) ;
	Enumeration create(Enumeration enumeration) throws ValidationException;
	Enumeration update(Enumeration enumeration) throws ValidationException;
	List<Enumeration> findByEnumerationName(String name);
	Page<Enumeration> findAllInPage(int page, int pageSize);
}
