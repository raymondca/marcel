package com.raymondchen.marcel.model.exception;

/**
 * A generic runtime exception which will be thrown to the outmost layer
 * @author Raymond
 *
 */
public class SystemException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SystemException(Throwable t) {
		super(t);
	}
}
