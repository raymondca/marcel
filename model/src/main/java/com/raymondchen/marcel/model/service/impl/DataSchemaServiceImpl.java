package com.raymondchen.marcel.model.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.raymondchen.marcel.model.DataSchema;
import com.raymondchen.marcel.model.dao.DataSchemaRepository;
import com.raymondchen.marcel.model.exception.ObjectNotFoundException;
import com.raymondchen.marcel.model.exception.ValidationException;
import com.raymondchen.marcel.model.service.DataSchemaService;
import com.raymondchen.marcel.model.validation.ModelValidator;

@Service
public class DataSchemaServiceImpl implements DataSchemaService {
	
	@Autowired
	private DataSchemaRepository dataSchemaRepository;
	
	@Autowired
	private ModelValidator modelValidator;

	@Override
	public DataSchema findById(Long id) throws ObjectNotFoundException {
		DataSchema ds=dataSchemaRepository.findOne(id);
		if (ds==null) {
			throw new ObjectNotFoundException("DataSchema not found for ID="+id);
		}
		return ds;
	}

	@Override
	public void deleteById(Long id) {
		dataSchemaRepository.delete(id);
	}

	@Override
	public List<DataSchema> list(int page, int pageSize) {
		return dataSchemaRepository.findAll(new PageRequest(page - 1, pageSize)).getContent();
	}

	@Override
	public DataSchema create(DataSchema dataSchema) throws ValidationException{
		modelValidator.validateNewDataSchema(dataSchema);
		return dataSchemaRepository.save(dataSchema);
	}

	@Override
	public DataSchema update(DataSchema dataSchema) throws ValidationException {
		modelValidator.validateUpdateDataSchema(dataSchema);
		return dataSchemaRepository.save(dataSchema);
	}

	@Override
	public List<DataSchema> findBySchemaName(String schemaName) {
		DataSchema example=new DataSchema();
		example.setName(schemaName);
		return dataSchemaRepository.findAll(Example.of(example));
	}

	@Override
	public Page<DataSchema> findAllInPage(int page, int pageSize) {
		Pageable pageable=new PageRequest(page-1,pageSize);
		return dataSchemaRepository.findAll(pageable);
	}

}
