package com.raymondchen.marcel.model.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.raymondchen.marcel.model.DataSchema;

/**
 * Repository class to access DataSchema data
 * @author Raymond
 *
 */
public interface DataSchemaRepository extends JpaRepository<DataSchema, Long> {

}
