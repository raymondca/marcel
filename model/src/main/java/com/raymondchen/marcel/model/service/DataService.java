package com.raymondchen.marcel.model.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.raymondchen.marcel.model.Data;
import com.raymondchen.marcel.model.exception.ObjectNotFoundException;
import com.raymondchen.marcel.model.exception.ValidationException;

public interface DataService {
	Data findById(Long id) throws ObjectNotFoundException;
	void deleteById(Long id);
	List<Data> list(int page, int pageSize) ;
	Data create(Data data) throws ValidationException;
	Data update(Data data) throws ValidationException;
	List<Data> findByDataSchemaId(Long dataSchemaId);
	Page<Data> findAllInPage(int page, int pageSize);
}
