package com.raymondchen.marcel.model.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.raymondchen.marcel.model.Data;
import com.raymondchen.marcel.model.dao.DataRepository;
import com.raymondchen.marcel.model.dao.DataRepositoryCustom;
import com.raymondchen.marcel.model.exception.ObjectNotFoundException;
import com.raymondchen.marcel.model.exception.ValidationException;
import com.raymondchen.marcel.model.service.DataService;
import com.raymondchen.marcel.model.validation.ModelValidator;

@Service
@Transactional
public class DataServiceImpl implements DataService {
	
	@Autowired
	private DataRepositoryCustom dataRepositoryCustom;
	
	@Autowired
	private DataRepository dataRepository;
	
	@Autowired
	private ModelValidator modelValidator;

	@Override
	public Data findById(Long id) throws ObjectNotFoundException {
		Data data= dataRepository.findOne(id);
		if (data==null) {
			throw new ObjectNotFoundException("Data not found for id="+id);
		}
		return data;
	}

	@Override
	public void deleteById(Long id) {
		dataRepository.delete(id);
	}

	@Override
	public List<Data> list(int page, int pageSize) {
		return dataRepository.findAll(new PageRequest(page-1,pageSize)).getContent();
	}

	@Override
	public Data create(Data data) throws ValidationException {
		modelValidator.validateNewData(data);
		return dataRepository.save(data);
	}

	@Override
	public Data update(Data data) throws ValidationException {
		modelValidator.validateUpdateData(data);
		return dataRepository.save(data);
	}
	
	@Override
	public List<Data> findByDataSchemaId(Long dataSchemaId ) {
		return dataRepositoryCustom.findByDataSchemaId(dataSchemaId);
	}

	@Override
	public Page<Data> findAllInPage(int page, int pageSize) {
		Pageable pageable=new PageRequest(page-1,pageSize);
		return dataRepository.findAll(pageable);
	}

}
