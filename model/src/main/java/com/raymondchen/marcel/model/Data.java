package com.raymondchen.marcel.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Entity class of 'data' table
 * @author Raymond
 *
 */
@Entity
public class Data {
	
	@JsonIgnore
	@ManyToOne()
	@JoinColumn(name="schemaid")
    private DataSchema dataSchema;
	
	@Id
	@GeneratedValue
	private Long id;
	
	@Column(name="createtime")
	private Date createTime;
	
	@Column(name="lastmodifiedtime")
	private Date lastModifiedTime;

	@Column(name="data")
	private String dataColumn;
	
	public DataSchema getDataSchema() {
		return dataSchema;
	}

	public void setDataSchema(DataSchema dataSchema) {
		this.dataSchema = dataSchema;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getLastModifiedTime() {
		return lastModifiedTime;
	}

	public void setLastModifiedTime(Date lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}

	public String getData() {
		return dataColumn;
	}

	public void setData(String data) {
		this.dataColumn = data;
	}

	

}
