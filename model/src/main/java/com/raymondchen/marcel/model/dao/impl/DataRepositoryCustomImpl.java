package com.raymondchen.marcel.model.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;

import com.raymondchen.marcel.model.Data;
import com.raymondchen.marcel.model.dao.DataRepositoryCustom;

@Service
public class DataRepositoryCustomImpl implements DataRepositoryCustom {
	@PersistenceContext
    private EntityManager em;
	
	@Override
	public List<Data> findByDataSchemaId(Long dataSchemaId) {
		String sql="select d from Data as d where d.dataSchema.id = :dataSchemaId";
		Query query=em.createQuery(sql);
		query.setParameter("dataSchemaId", dataSchemaId);
		return query.getResultList();
	}

}
