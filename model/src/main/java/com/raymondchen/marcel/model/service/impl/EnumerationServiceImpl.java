package com.raymondchen.marcel.model.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.raymondchen.marcel.model.Enumeration;
import com.raymondchen.marcel.model.dao.EnumerationRepository;
import com.raymondchen.marcel.model.exception.ObjectNotFoundException;
import com.raymondchen.marcel.model.exception.ValidationException;
import com.raymondchen.marcel.model.service.EnumerationService;
import com.raymondchen.marcel.model.validation.ModelValidator;

@Service
public class EnumerationServiceImpl implements EnumerationService {

	@Autowired
	private EnumerationRepository enumerationRepository;
	
	@Autowired
	private ModelValidator modelValidator;
	
	@Override
	public Enumeration findById(Long id) throws ObjectNotFoundException {
		Enumeration enu= enumerationRepository.findOne(id);
		if (enu==null) {
			throw new ObjectNotFoundException("Enumeration not found for id="+id);
		}
		return enu;
	}

	@Override
	public void deleteById(Long id) {
		enumerationRepository.delete(id);
	}

	@Override
	public List<Enumeration> list(int page, int pageSize) {
		return enumerationRepository.findAll(new PageRequest(page-1,pageSize)).getContent();
	}

	@Override
	public Enumeration create(Enumeration enumeration) throws ValidationException {
		Enumeration enu=new Enumeration();
		enu.setName(enumeration.getName());
		enu.setValue(enumeration.getValue());
		modelValidator.validateNewEnum(enu);
		return enumerationRepository.save(enu);
	}

	@Override
	public Enumeration update(Enumeration enumeration) throws ValidationException {
		Enumeration enu=enumerationRepository.findOne(enumeration.getId());
		enu.setName(enumeration.getName());
		enu.setValue(enumeration.getValue());
		modelValidator.validateUpdateEnum(enu);
		return enumerationRepository.save(enu);
	}

	@Override
	public List<Enumeration> findByEnumerationName(String name) {
		Enumeration example=new Enumeration();
		example.setName(name);
		return enumerationRepository.findAll(Example.of(example));
	}

	@Override
	public Page<Enumeration> findAllInPage(int page, int pageSize) {
		Pageable pageable=new PageRequest(page-1,pageSize);
		return enumerationRepository.findAll(pageable);
	}

}
