package com.raymondchen.marcel.model.validation;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.raymondchen.marcel.model.Data;
import com.raymondchen.marcel.model.DataSchema;
import com.raymondchen.marcel.model.Enumeration;
import com.raymondchen.marcel.model.dao.DataSchemaRepository;
import com.raymondchen.marcel.model.dao.EnumerationRepository;
import com.raymondchen.marcel.model.exception.ValidationException;

@Service
public class ModelValidator {
	
	public static final String KEY_ERROR_JSON_FORMAT_ERROR="ErrorJsonFormatError";
	public static final String KEY_ERROR_NULL_PARAMETER="ErrorNullParameter";
	
	public static final String KEY_ERROR_ENUMERATION_NAME_EXIST="ErrorEnumerationNameExist";
	public static final String KEY_ERROR_ENUMERATION_HAS_DUPLICATE="ErrorEnumerationHasDuplicate";
	public static final String KEY_ERROR_ENUMERATION_NAME_OR_VALUE_EMPTY="ErrorEnumerationNameOrValueEmpty";
	
	public static final String KEY_ERROR_DATA_SCHEMA_NAME_EXIST="ErrorDataSchemaNameExist";
	public static final String KEY_ERROR_DATA_SCHEMA_FIELD_NAME_HAS_DUPLICATE="ErrorDataSchemaFieldNameHasDuplicate";
	public static final String KEY_ERROR_DATA_SCHEMA_NAME_OR_DEFINITION_EMPTY="ErrorDataSchemaNameOrDefinitionEmpty";
	
	@Autowired
	private EnumerationRepository enumerationRepository;
	
	@Autowired
	private DataSchemaRepository dataSchemaRepository;
	
	/**
	 * Throw a ValidationException if failed.
	 * Rules:
	 * 	1) Neither of name or value is empty
	 *  2) Value is a json of string array without duplicate items
	 *  3) No duplicate entry in value list
	 *  4) Name doesn't exist
	 */
	public void validateNewEnum(Enumeration newEnum) throws ValidationException {
		if (StringUtils.isEmpty(newEnum.getName()) || StringUtils.isEmpty(newEnum.getValue())) {
			throw new ValidationException(KEY_ERROR_ENUMERATION_NAME_OR_VALUE_EMPTY, new String[]{});
		}
		JSONArray json=getJsonArray(newEnum.getValue());
		Set<Object> set=new HashSet<>();
		for (int i=0;i<json.length();i++) {
			if (!set.add(json.get(i))) {
				throw new ValidationException(KEY_ERROR_ENUMERATION_HAS_DUPLICATE,new String[]{json.get(i).toString()});
			}
		}
		Enumeration existingEnum=new Enumeration();
		existingEnum.setName(newEnum.getName());
		List<Enumeration> list=enumerationRepository.findAll(Example.of(existingEnum));
		if (!list.isEmpty()) {
			throw new ValidationException(KEY_ERROR_ENUMERATION_NAME_EXIST, new String[]{newEnum.getName()});
		}
	}
	
	/**
	 * Throw a ValidationException if failed.
	 * Rules:
	 * 	1) Neither of name or value is empty
	 *  2) Value is a json of string array without duplicate items
	 *  3) No duplicate entry in value list
	 *  4) No other Enumeration has the same name
	 */
	public void validateUpdateEnum(Enumeration updateEnum) throws ValidationException  {
		if (StringUtils.isEmpty(updateEnum.getName()) || StringUtils.isEmpty(updateEnum.getValue())) {
			throw new ValidationException(KEY_ERROR_ENUMERATION_NAME_OR_VALUE_EMPTY, new String[]{});
		}
		JSONArray json=getJsonArray(updateEnum.getValue());
		Set<Object> set=new HashSet<>();
		for (int i=0;i<json.length();i++) {
			if (!set.add(json.get(i))) {
				throw new ValidationException(KEY_ERROR_ENUMERATION_HAS_DUPLICATE,new String[]{json.get(i).toString()});
			}
		}
		Enumeration existingEnum=new Enumeration();
		existingEnum.setName(updateEnum.getName());
		List<Enumeration> list=enumerationRepository.findAll(Example.of(existingEnum));
		if (!list.stream().filter(
					e -> !e.getId().equals(updateEnum.getId()))
				.collect(Collectors.toList())
				.isEmpty()
				) {
			throw new ValidationException(KEY_ERROR_ENUMERATION_NAME_EXIST, new String[]{updateEnum.getName()});
		}
	}
	
	/**
	 * Throw a ValidationException if failed.
	 * Rules:
	 * 1) Neither name or definition is empty
	 * 2) Name of the new DataSchema should not exist already
	 * 3) Definition should be a valid JSON array
	 * 4) There should not be duplicate field name in the definition array
	 * @param dataSchema
	 */
	public void validateNewDataSchema(DataSchema dataSchema) throws ValidationException {
		if (StringUtils.isEmpty(dataSchema.getName()) || StringUtils.isEmpty(dataSchema.getDefinition())) {
			throw new ValidationException(KEY_ERROR_DATA_SCHEMA_NAME_OR_DEFINITION_EMPTY, new String[]{});
		}
		JSONArray json=getJsonArray(dataSchema.getDefinition());
		// Value "[]" is also invalid
		if (json.length()==0) {
			throw new ValidationException(KEY_ERROR_DATA_SCHEMA_NAME_OR_DEFINITION_EMPTY, new String[]{});
		}
		Set<Object> set=new HashSet<>();
		for (int i=0;i<json.length();i++) {
			if (!set.add(json.getJSONObject(i).get("name"))) {
				throw new ValidationException(KEY_ERROR_DATA_SCHEMA_FIELD_NAME_HAS_DUPLICATE,new String[]{json.get(i).toString()});
			}
		}
		DataSchema ds=new DataSchema();
		ds.setName(dataSchema.getName());
		List<DataSchema> list=dataSchemaRepository.findAll(Example.of(ds));
		if (list.size()>0) {
			throw new ValidationException(KEY_ERROR_DATA_SCHEMA_NAME_EXIST,new String[]{dataSchema.getName()});
		}
	}
	
	/**
	 * Throw a ValidationException if failed.
	 * Rules:
	 * 1) Neither name or definition is empty
	 * 2) Name of the new DataSchema should not exist already other than itself
	 * 3) Definition should be a valid JSON array
	 * 4) There should not be duplicate field name in the definition array
	 * @param dataSchema
	 */
	public void validateUpdateDataSchema(DataSchema dataSchema) throws ValidationException {
		if (StringUtils.isEmpty(dataSchema.getName()) || StringUtils.isEmpty(dataSchema.getDefinition())) {
			throw new ValidationException(KEY_ERROR_DATA_SCHEMA_NAME_OR_DEFINITION_EMPTY, new String[]{});
		}
		JSONArray json=getJsonArray(dataSchema.getDefinition());
		// Value "[]" is also invalid
		if (json.length()==0) {
			throw new ValidationException(KEY_ERROR_DATA_SCHEMA_NAME_OR_DEFINITION_EMPTY, new String[]{});
		}
		Set<Object> set=new HashSet<>();
		for (int i=0;i<json.length();i++) {
			if (!set.add(json.getJSONObject(i).get("name"))) {
				throw new ValidationException(KEY_ERROR_DATA_SCHEMA_FIELD_NAME_HAS_DUPLICATE,new String[]{json.get(i).toString()});
			}
		}
		DataSchema ds=new DataSchema();
		ds.setName(dataSchema.getName());
		List<DataSchema> list=dataSchemaRepository.findAll(Example.of(ds));
		if (!list.stream().filter(
				e -> !e.getId().equals(dataSchema.getId()))
			.collect(Collectors.toList())
			.isEmpty()
			) {
		throw new ValidationException(KEY_ERROR_DATA_SCHEMA_NAME_EXIST, new String[]{dataSchema.getName()});
	}
	}
	
	/**
	 * Throw a ValidationException if json text is not well-formed
	 * @param text
	 * @throws ValidationException
	 */
	private JSONArray getJsonArray(String text) throws ValidationException {
		try {
		  return new JSONArray(text);
		} catch (JSONException e) {
			throw new ValidationException(KEY_ERROR_JSON_FORMAT_ERROR,new String[]{e.getMessage()});
		}
	}
	
	public void validateNewData(Data data) throws ValidationException {
		if (data==null) {
			throw new ValidationException(KEY_ERROR_NULL_PARAMETER,new String[]{});
		}
		if (data.getDataSchema()==null) {
			throw new ValidationException(KEY_ERROR_NULL_PARAMETER,new String[]{});
		}
		JSONArray json=getJsonArray(data.getData());
		for (int i=0;i<json.length();i++) {
			JSONObject o=json.getJSONObject(i);
			if (StringUtils.isEmpty(o.get("name")) || StringUtils.isEmpty(o.get("value"))) {
				throw new ValidationException(KEY_ERROR_JSON_FORMAT_ERROR,new String[]{});
			}
		}
	}
	
	public void validateUpdateData(Data data) throws ValidationException {
		if (data==null) {
			throw new ValidationException(KEY_ERROR_NULL_PARAMETER,new String[]{});
		}
		if (data.getDataSchema()==null) {
			throw new ValidationException(KEY_ERROR_NULL_PARAMETER,new String[]{});
		}
		JSONArray json=getJsonArray(data.getData());
		for (int i=0;i<json.length();i++) {
			JSONObject o=json.getJSONObject(i);
			if (StringUtils.isEmpty(o.get("name")) || StringUtils.isEmpty(o.get("value"))) {
				throw new ValidationException(KEY_ERROR_JSON_FORMAT_ERROR,new String[]{});
			}
		}
	}
}
