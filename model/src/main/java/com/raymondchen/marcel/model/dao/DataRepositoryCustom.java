package com.raymondchen.marcel.model.dao;

import java.util.List;

import com.raymondchen.marcel.model.Data;

public interface DataRepositoryCustom {
	List<Data> findByDataSchemaId(Long dataSchemaId);
}
