package com.raymondchen.marcel;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Entry class of REST API
 * @author Raymond
 *
 */
@Configuration
@ComponentScan
@EnableAutoConfiguration
public class Application {
private Log logger = LogFactory.getLog(Application.class);
	@Bean
	CommandLineRunner init() {
		return evt -> logger.debug("Application.init("); // Do nothing
	}

	/**
	 * Main
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}




