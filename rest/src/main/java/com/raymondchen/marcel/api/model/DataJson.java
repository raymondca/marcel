package com.raymondchen.marcel.api.model;

import com.raymondchen.marcel.model.Data;

public class DataJson extends Data {
	private Long schemaId;
	public Long getSchemaId() {
		return this.getDataSchema()!=null?this.getDataSchema().getId():this.schemaId;
	}
	
	public void setSchemaId(Long schemaId) {
		this.schemaId=schemaId;
	}
}
