package com.raymondchen.marcel.api.controller;

public interface Version {
	int API_VERSION = 1;
	String API_PREFIX = "/api/" + API_VERSION;
}
