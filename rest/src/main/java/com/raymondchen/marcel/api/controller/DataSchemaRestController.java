package com.raymondchen.marcel.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.raymondchen.marcel.api.util.Defaults;
import com.raymondchen.marcel.model.DataSchema;
import com.raymondchen.marcel.model.exception.ObjectNotFoundException;
import com.raymondchen.marcel.model.exception.SystemException;
import com.raymondchen.marcel.model.exception.ValidationException;
import com.raymondchen.marcel.model.service.DataSchemaService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(DataSchemaRestController.URL_PREFIX)
public class DataSchemaRestController {
	public static final String URL_PREFIX = Version.API_PREFIX + "/schema";

	@Autowired
	private DataSchemaService dataSchemaService;

	@RequestMapping(value = "/findByName/{schemaName}", method = RequestMethod.GET)
	public List<DataSchema> findBySchemaName(@PathVariable String schemaName) {
		return dataSchemaService.findBySchemaName(schemaName);
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public List<DataSchema> listDataSchemas(
			@RequestParam(value = "page", required = false, defaultValue = "1") int page,
			@RequestParam(value = "pageSize", required = false, defaultValue = Defaults.PAGE_SIZE + "") int pageSize) {
		return dataSchemaService.list(page, pageSize);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public DataSchema findById(@PathVariable long id) {
		try {
			return dataSchemaService.findById(id);
		} catch (ObjectNotFoundException e) {
			throw new SystemException(e);
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public boolean delete(@PathVariable Long id) {
		dataSchemaService.deleteById(id);
		return true;
	}

	@RequestMapping(value = "/", method = RequestMethod.PUT)
	public DataSchema update(@RequestBody(required = true) DataSchema dataSchema) throws ValidationException {
		DataSchema ds;
		try {
			ds = dataSchemaService.findById(dataSchema.getId());
		} catch (ObjectNotFoundException e) {
			throw null;
		}
		ds.setName(dataSchema.getName());;
		ds.setDefinition(dataSchema.getDefinition());
		return dataSchemaService.update(ds);
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public DataSchema create(@RequestBody(required = true) DataSchema dataSchema)  throws ValidationException {
		return dataSchemaService.create(dataSchema);
	}
}
