package com.raymondchen.marcel.api.model;


public class RestError {
	public static final int KEY_ERROR_ENUMERATION_HAS_DUPLICATE=1000;
	public static final int KEY_ERROR_ENUMERATION_NAME_EXIST=1001;
	private Integer code;
	private String description;
	private String data;
	public RestError(Integer code, String description, String data) {
		super();
		this.code = code;
		this.description = description;
		this.data = data;
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	
}
