package com.raymondchen.marcel.api.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.raymondchen.marcel.api.model.RestError;
import com.raymondchen.marcel.api.util.Defaults;
import com.raymondchen.marcel.model.Enumeration;
import com.raymondchen.marcel.model.exception.ObjectNotFoundException;
import com.raymondchen.marcel.model.exception.SystemException;
import com.raymondchen.marcel.model.exception.ValidationException;
import com.raymondchen.marcel.model.service.EnumerationService;
import com.raymondchen.marcel.model.validation.ModelValidator;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(EnumerationRestController.URL_PREFIX)
public class EnumerationRestController {
	public static final String URL_PREFIX = Version.API_PREFIX + "/enum";

	@Autowired
	private EnumerationService enumerationService;

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(ValidationException.class)
	public RestError handleConflict(ValidationException exception) {
		if (exception.getKey().equals(ModelValidator.KEY_ERROR_ENUMERATION_NAME_EXIST)) {
			RestError error = new RestError(RestError.KEY_ERROR_ENUMERATION_NAME_EXIST,"Duplicate enum name","");
			return error;
		}
		RestError error = new RestError(0,"Unknown error","");
		return error;
	}

	@RequestMapping(value = "/findByName/{enumName}", method = RequestMethod.GET)
	public List<Enumeration> findByEnumerationName(@PathVariable String enumName)
			throws ValidationException {
		return enumerationService.findByEnumerationName(enumName);
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public List<Enumeration> listEnumerations(
			@RequestParam(value = "page", required = false, defaultValue = "1") int page,
			@RequestParam(value = "pageSize", required = false, defaultValue = Defaults.PAGE_SIZE + "") int pageSize) {
		return enumerationService.list(page, pageSize);
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Enumeration create(@RequestBody(required = true) Enumeration enumeration) throws ValidationException {
		return enumerationService.create(enumeration);
	}

	@RequestMapping(value = "/", method = RequestMethod.PUT)
	public Enumeration update(@RequestBody(required = true) Enumeration enumeration) throws ValidationException {
		return enumerationService.update(enumeration);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public boolean delete(@PathVariable Long id) {
		enumerationService.deleteById(id);
		return true;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Enumeration findById(@PathVariable long id) {
		try {
			return enumerationService.findById(id);
		} catch (ObjectNotFoundException e) {
			throw new SystemException(e);
		}
	}
}
