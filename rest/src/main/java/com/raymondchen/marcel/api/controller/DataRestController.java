package com.raymondchen.marcel.api.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.raymondchen.marcel.api.model.DataJson;
import com.raymondchen.marcel.api.util.Defaults;
import com.raymondchen.marcel.model.Data;
import com.raymondchen.marcel.model.exception.ObjectNotFoundException;
import com.raymondchen.marcel.model.exception.SystemException;
import com.raymondchen.marcel.model.exception.ValidationException;
import com.raymondchen.marcel.model.service.DataSchemaService;
import com.raymondchen.marcel.model.service.DataService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(DataRestController.URL_PREFIX)
public class DataRestController {
	public static final String URL_PREFIX = Version.API_PREFIX + "/data";

	@Autowired
	private DataService dataService;
	
	@Autowired
	private DataSchemaService dataSchemaService;

	/**
	 * Read all data list
	 * 
	 * @return
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public List<DataJson> listDatas(@RequestParam(value = "page", required = false, defaultValue = "1") int page,
			@RequestParam(value = "pageSize", required = false, defaultValue = Defaults.PAGE_SIZE + "") int pageSize) {
		List<Data> list= dataService.list(page, pageSize);
		return list.stream().map( data -> {
			DataJson dj=new DataJson();
			dj.setId(data.getId());
			dj.setData(data.getData());
			dj.setCreateTime(data.getCreateTime());
			dj.setLastModifiedTime(data.getLastModifiedTime());
			dj.setDataSchema(data.getDataSchema());
			return dj;
		}).collect(Collectors.toList());
	}

	/**
	 * Read data list of a dataSchemaId
	 * 
	 * @param dataSchemaId
	 * @return
	 */
	@RequestMapping(value = "/findBySchemaId/{dataSchemaId}", method = RequestMethod.GET)
	public List<Data> findBySchemaId(@PathVariable Long dataSchemaId) {
		return dataService.findByDataSchemaId(dataSchemaId);
	}

	@RequestMapping(value = "/", method = RequestMethod.PUT)
	public Data update(@RequestBody(required = true) Data data) throws ValidationException {
		try {
			Data dbData=dataService.findById(data.getId());
			dbData.setLastModifiedTime(data.getLastModifiedTime());;
			dbData.setData(data.getData());
			return dataService.update(dbData);
		} catch (ObjectNotFoundException e) {
			throw new ValidationException(e.getMessage(),null);
		}
		
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Data create(@RequestBody(required = true) DataJson data) throws ValidationException {
		Data d=new Data();
		try {
			d.setCreateTime(data.getCreateTime());
			d.setData(data.getData());
			d.setLastModifiedTime(data.getLastModifiedTime());
			d.setDataSchema(dataSchemaService.findById(data.getSchemaId()));
		} catch (ObjectNotFoundException e) {
			throw new ValidationException(e.getMessage(),null);
		}
		return dataService.create(d);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public boolean delete(@PathVariable Long id) {
		try {
			dataService.findById(id);
		} catch (ObjectNotFoundException e) {
			return false;
		}
		dataService.deleteById(id);
		return true;
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public DataJson findById(@PathVariable long id) {
		try {
			Data d= dataService.findById(id);
			DataJson result=new DataJson();
			result.setCreateTime(d.getCreateTime());;
			result.setData(d.getData());
			result.setId(d.getId());
			result.setLastModifiedTime(d.getLastModifiedTime());
			result.setSchemaId(d.getDataSchema().getId());
			return result;
		} catch (ObjectNotFoundException e) {
			throw new SystemException(e);
		}
	}
}
