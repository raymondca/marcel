package com.raymondchen.marcel;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.raymondchen.marcel.test.BaseTestCase;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class ApplicationTest extends BaseTestCase {
	private Log logger = LogFactory.getLog(ApplicationTest.class);
	@Test
	public void sanity() {
		logger.info("ApplicationTest is running...");
		Assert.assertTrue("Simply making sure application context is initialized properly",true);
	}
}
