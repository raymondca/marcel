package com.raymondchen.marcel.api.controller;

import java.util.Collection;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.raymondchen.marcel.Application;
import com.raymondchen.marcel.api.model.DataJson;
import com.raymondchen.marcel.model.Data;
import com.raymondchen.marcel.test.BaseTestCase;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class DataRestControllerTest extends BaseTestCase {
	
	@Autowired
	private DataRestController dataRestController;

	@Test
	public void testListDatas() {
		List<DataJson> list=dataRestController.listDatas(1,50);
		Assert.assertTrue(list.size()>0);
	}

	@Test
	public void testFindBySchemaId() {
		Collection<Data> list=dataRestController.findBySchemaId(super.dataSchemaId);
		Assert.assertEquals(1, list.size());
	}

}
