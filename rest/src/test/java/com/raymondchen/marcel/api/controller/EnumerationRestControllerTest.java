package com.raymondchen.marcel.api.controller;


import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.raymondchen.marcel.Application;
import com.raymondchen.marcel.model.Enumeration;
import com.raymondchen.marcel.model.dao.EnumerationRepositoryCustom;
import com.raymondchen.marcel.model.exception.ValidationException;
import com.raymondchen.marcel.test.BaseTestCase;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class EnumerationRestControllerTest extends BaseTestCase {

	@Autowired
	private EnumerationRestController enumerationRestController;
	
	@Autowired
	private EnumerationRepositoryCustom enumerationRepositoryCustom;
	
	@Test
	public void testFindBySchemaName() throws ValidationException {
		List<Enumeration> list=enumerationRestController.findByEnumerationName(super.enumerationName);
		Assert.assertNotNull(list);
		Assert.assertEquals(1, list.size());
	}
	
	@Test
	public void testListEnumerations() {
		List<Enumeration> list=enumerationRestController.listEnumerations(1, 100);
		Assert.assertFalse(list.isEmpty());
	}

	@Test
	public void testCreate()  throws ValidationException{
		Enumeration e=new Enumeration();
		e.setName("UnitTestEnumName001");
		e.setValue( "[\"abc\",\"def\"]");
		Enumeration enu=enumerationRestController.create(e);
		Assert.assertNotNull(enu.getId());
	}
	
	@Test
	public void testUpdate()  throws ValidationException{
		Enumeration enu=enumerationRestController.findByEnumerationName(super.enumerationName).get(0);
		enu.setName(enu.getName()+"_Changed");
		enu.setValue("[\"1222\",\"kkko\"]");
		enumerationRestController.update(enu);
		Assert.assertNotNull(enu.getId());
		List<Enumeration>  list=enumerationRestController.findByEnumerationName(super.enumerationName);
		Assert.assertTrue(list.isEmpty());
	}
	
	@Test
	public void testDelete() throws ValidationException {
		Enumeration enu=enumerationRestController.findByEnumerationName(super.enumerationName).get(0);
		enumerationRestController.delete(enu.getId());
		List<Enumeration>  list=enumerationRestController.findByEnumerationName(super.enumerationName);
		Assert.assertTrue(list.isEmpty());
	}
	
	@Test
	public void testFindById() {
		Enumeration enu=enumerationRestController.findById(super.enumerationId);
		Assert.assertNotNull(enu);
		Assert.assertEquals(super.enumerationName, enu.getName());
	}
}
