package com.raymondchen.marcel.api.controller;


import java.util.Collection;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.raymondchen.marcel.Application;
import com.raymondchen.marcel.model.DataSchema;
import com.raymondchen.marcel.test.BaseTestCase;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class DataSchemaRestControllerTest extends BaseTestCase {
	
	@Autowired
	DataSchemaRestController dataSchemaRestController;

	@Test
	public void testFindBySchemaName() {
		List<DataSchema> list=dataSchemaRestController.findBySchemaName(super.dataSchemaName);
		Assert.assertEquals(1,list.size());
	}
	
	@Test
	public void testListDataSchemas() {
		Collection<DataSchema> list=dataSchemaRestController.listDataSchemas(1, 22);
		Assert.assertFalse(list.isEmpty());;
	}

}
