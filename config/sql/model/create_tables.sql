create table enumeration (id bigint primary key auto_increment,name varchar(100),value varchar(100));

create table dataschema (id bigint primary key auto_increment, name varchar(100), version bigint default 1, definition varchar(5000), constraint idx_dataschema_name_version unique (name,version));

create table data (id bigint primary key auto_increment ,createtime timestamp default CURRENT_TIMESTAMP,lastmodifiedtime datetime, schemaid bigint, data text, constraint foreign key fk_data_schemaid (schemaid) references dataschema(id) on delete cascade);
