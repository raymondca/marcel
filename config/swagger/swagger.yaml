openapi: '3.0.0'

info:
  version: "1.0.0"
  title: "Marcel API"
  description: Life is a boxing game… Defeat is NOT declared when you fall down; It is declared when you refuse to ‘Get Up’!
  contact:
    name: Engineering
    email: 'raymondchen625@gmail.com'
  license:
    name: "MIT"
    url: "https://opensource.org/licenses/MIT"

tags:
  - name: Enumeration
    description: Enumeration operations
    externalDocs:
      description: Marcel on BitBucket
      url: "https://bitbucket.org/raymondca/marcel"
  - name: DataSchema
    description: DataSchema  operations
    externalDocs:
      description: Marcel on BitBucket
      url: "https://bitbucket.org/raymondca/marcel"
  - name: Data
    description: Data operations
    externalDocs:
      description: Marcel on BitBucket
      url: "https://bitbucket.org/raymondca/marcel"

servers:
  - url: http://marcel-api:1973/api/1
    description: Test/Dev Endpoint
  - url: http://api.marcel.raymondchen.com/v1
    description: Production Endpoint


paths:                
  /enum/findByName/{enumName}:
    get:
      summary: Search Enumeration by name
      description: |
        Gets `Enumeration` objects by specifying their name as the search criteria
      tags:
        - Enumeration
      parameters:
          -
            in: path
            name: enumName
            required: true
            schema:
              type: string
            description: Enumeration name
      responses:
          '200':
            description: Successful operation
            content:
              application/json:
                schema:
                  type: array
                  items:
                    $ref: '#/components/schemas/Enumeration'
  /enum/:
    get:
      summary: List Enumeration
      description: |
        Gets `Enumeration` objects as an Array.
      tags:
        - Enumeration
      parameters:
        -
          in: query
          name: page
          schema:
            type: integer
          required: false
          description: Page number starting from 1
        -
          in: query
          name: pageSize
          schema:
            type: integer
          required: false
          description: Page size of the pagination, default = 50
      responses:
        '200':
          description: Successful response
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Enumeration'
        '400':
            description: Invalid `page` or `pageSize` parameters provided
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/Error'
    post:
      summary: Create Enumeration
      description: Creates a new `Enumeration` object
      tags:
        - Enumeration
      requestBody:
        description: An `Enumeration` json object
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Enumeration'
      responses:
        '200':
          description: Enumeration successfully created
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Enumeration'
        '406':
          description: Enumeration creation failed due to enumeration name empty, existing or there are duplicate entries in the Enumeration
          content:
              application/json:
                schema:
                  $ref: '#/components/schemas/Error'
    put:
      summary: Update Enumeration
      description: Updates a new `Enumeration` object
      tags:
        - Enumeration
      requestBody:
        description: An `Enumeration` json object
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Enumeration'
      responses:
        '200':
          description: Successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Enumeration'
        '406':
          description: Enumeration creation failed due to enumeration name empty, existing or there are duplicate entries in the Enumeration
          content:
              application/json:
                schema:
                  $ref: '#/components/schemas/Error'
  /enum/{id}:
    delete:
      summary: Delete Enumeration
      description: Delete an `Enumeration`
      tags:
        - Enumeration
      parameters:
        -
          in: path
          name: id
          required: true
          schema:
            type: integer
          description: Enumeration ID
      responses:
        '200':
          description: Successful operation
          content:
            application/json:
              schema:
                type: boolean
        '404':
          description: Enumeration deletion failed due to Enumeration ID does not exist
          content:
              application/json:
                schema:
                  $ref: '#/components/schemas/Error'
        '400':
          description: Invalid id provided
          content:
              application/json:
                schema:
                  $ref: '#/components/schemas/Error'
    get:
      summary: Get Enumeration
      description: Gets the `Enumeration` object by specifying its ID in the path parameter
      tags:
        - Enumeration
      parameters:
          -
            in: path
            name: id
            required: true
            schema:
              type: integer
            description: Enumeration ID
      responses:
          '200':
            description: Successful operation
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/Enumeration'
          '400':
            description: Invalid id provided
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/Error'
          '404':
            description: Object not found with specified `id`
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/Error'
  /schema/findByName/{schemaName}:
    get:
      summary: Search DataSchema by Name
      description: Gets `DataSchema` objects by specifying their name as the search criteria
      tags:
        - DataSchema
      parameters:
          -
            in: path
            name: schemaName
            required: true
            schema:
              type: string
            description: DataSchema name
      responses:
          '200':
            description: Successful operation
            content:
              application/json:
                schema:
                  type: array
                  items:
                    $ref: '#/components/schemas/DataSchema'
  /schema/:
    get:
      summary: List DataSchema
      description: Gets `DataSchema` objects as an Array.
      tags:
        - DataSchema
      parameters:
        -
          in: query
          name: page
          schema:
            type: integer
          required: false
          description: Page number starting from 1
        -
          in: query
          name: pageSize
          schema:
            type: integer
          required: false
          description: Page size of the pagination, default = 50
      responses:
        '200':
          description: Successful response
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/DataSchema'
    post:
      summary: Create DataSchema
      description: Creates a new `DataSchema` object
      tags:
        - DataSchema
      requestBody:
        description: An `DataSchema` json object
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/DataSchema'
      responses:
        '200':
          description: DataSchema successfully created
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/DataSchema'
    put:
      summary: Update DataSchema
      description: Updates a new `DataSchema` object
      tags:
        - DataSchema
      requestBody:
        description: An `DataSchema` json object
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/DataSchema'
      responses:
        '200':
          description: DataSchema successfully updated
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/DataSchema'
  /schema/{id}:
    delete:
      summary: Delete DataSchema
      description: Delete an `DataSchema`
      tags:
        - DataSchema
      parameters:
        -
          in: path
          name: id
          required: true
          schema:
            type: integer
          description: DataSchema ID
      responses:
        '200':
          description: The delete result as a boolean
          content:
            application/json:
              schema:
                type: boolean
    get:
      summary: Get DataSchema
      description: Gets the `DataSchema` object by specifying its ID in the path parameter
      tags:
        - DataSchema
      parameters:
          -
            in: path
            name: id
            required: true
            schema:
              type: integer
            description: DataSchema ID
      responses:
          '200':
            description: DataSchema object
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/DataSchema'
          '400':
            description: Invalid id provided
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/Error'
          '404':
            description: Object not found with specified `id`
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/Error'
  /data/:
    get:
      summary: List Data
      description: Gets `Data` objects as an Array.
      tags:
        - Data
      parameters:
        -
          in: query
          name: page
          schema:
            type: integer
          required: false
          description: Page number starting from 1
        -
          in: query
          name: pageSize
          schema:
            type: integer
          required: false
          description: Page size of the pagination, default = 50
      responses:
        '200':
          description: Successful response
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Data'
    post:
      summary: Create Data
      description: Creates a new `Data` object
      tags:
        - Data
      requestBody:
        description: An `Data` json object
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Data'
      responses:
        '200':
          description: Data successfully created
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Data'
    put:
      summary: Update Data
      description: Updates a new `Data` object
      tags:
        - Data
      requestBody:
        description: An `Data` json object
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Data'
      responses:
        '200':
          description: Data successfully updated
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Data'
  /data/{id}:
    delete:
      summary: Detel Data
      description: Delete an `Data`
      tags:
        - Data
      parameters:
        -
          in: path
          name: id
          required: true
          schema:
            type: integer
          description: Data ID
      responses:
        '200':
          description: The delete result as a boolean
          content:
            application/json:
              schema:
                type: boolean
    get:
      summary: Get Data
      description: Gets the `Data` object by specifying its ID in the path parameter
      tags:
        - Data
      parameters:
          -
            in: path
            name: id
            required: true
            schema:
              type: integer
            description: Data ID
      responses:
          '200':
            description: Data object
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/Data'
          '400':
            description: Invalid id provided
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/Error'
          '404':
            description: Object not found with specified `id`
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/Error'
  /data/findBySchemaId/{dataSchemaId}:
    get:
      summary: Search Data by DataSchema id
      description: Gets `Data` objects by specifying their DataSchema ID as the search criteria
      tags:
        - Data
      parameters:
          -
            in: path
            name: dataSchemaId
            required: true
            schema:
              type: integer
            description: DataSchema ID
      responses:
          '200':
            description: Data array
            content:
              application/json:
                schema:
                  type: array
                  items:
                    $ref: '#/components/schemas/Data'
components:
  schemas:
    Enumeration:
      type: object
      properties:
          id:
            type: integer
            format: int32
          name:
            type: string
          value:
            type: string
    DataSchema:
      type: object
      properties:
          id:
            type: integer
            format: int32
          name:
            type: string
          version:
            type: integer
            format: int32
          definition:
            type: string
    Data:
      type: object
      properties:
        id:
          type: integer
          format: int32
        createTime:
          type: integer
          format: int32
        lastModifiedTime:
          type: integer
          format: int32
        data:
          type: string
    Error:
        type: object
        properties:
          code:
            type: integer
            format: int32
          description:
            type: string
          data:
            type: object