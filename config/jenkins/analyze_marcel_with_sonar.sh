#!/bin/bash
cd /home/ubuntu/marcel
git pull | grep 'Already up-to-date'
if [ $? == 1 ]
then
	echo "RUNNING" > /home/ubuntu/sonnar-scan-status.txt
	sonar-scanner
	echo "Done: `date`" > /home/ubuntu/sonnar-scan-status.txt
	curl -X GET "http://alphaca.duckdns.org/event/notify-jenkins-finish"
fi