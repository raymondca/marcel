#!/bin/bash
docker run --name=marcel -d -e MYSQL_ROOT_PASSWORD=alpha mysql:5.7
docker run --name jenkins-mysql -d --link marcel -p 8080:8080 -p 50000:50000 raymondchen625/jenkins-mysql