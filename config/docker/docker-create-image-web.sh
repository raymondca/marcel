#!/bin/bash
mkdir -p ./temp/marcel-web
cp marcel-web/Dockerfile ./temp/marcel-web
cd ./temp/marcel-web
git clone git@bitbucket.org:raymondca/marcel.git
docker build -t raymondchen625/marcel-web .
cd ../..
rm -rf ./temp