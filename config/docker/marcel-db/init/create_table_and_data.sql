-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: marcel
-- ------------------------------------------------------
-- Server version	5.7.13-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `data`
--

DROP TABLE IF EXISTS `data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastmodifiedtime` datetime DEFAULT NULL,
  `schemaid` bigint(20) DEFAULT NULL,
  `data` text,
  PRIMARY KEY (`id`),
  KEY `fk_data_schemaid` (`schemaid`),
  CONSTRAINT `data_ibfk_1` FOREIGN KEY (`schemaid`) REFERENCES `dataschema` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data`
--

LOCK TABLES `data` WRITE;
/*!40000 ALTER TABLE `data` DISABLE KEYS */;
INSERT INTO `data` VALUES (1,'2016-12-23 00:09:20','2016-12-22 19:09:20',1,'[{\"name\":\"Place\",\"value\":\"KFC\"},{\"name\":\"Comment\",\"value\":\"Good\"},{\"name\":\"Rating\",\"value\":\"4 Star\"}]'),(2,'2016-12-23 00:09:30','2016-12-22 19:09:30',1,'[{\"name\":\"Place\",\"value\":\"Mary Brown\"},{\"name\":\"Comment\",\"value\":\"ok\"},{\"name\":\"Rating\",\"value\":\"1 Star\"}]'),(3,'2016-12-23 00:09:45','2016-12-22 19:09:45',2,'[{\"name\":\"Color\",\"value\":\"Blue\"},{\"name\":\"Price\",\"value\":30},{\"name\":\"Brand\",\"value\":\"Old Navy\"}]'),(4,'2016-12-23 00:10:07','2016-12-22 19:10:07',2,'[{\"name\":\"Color\",\"value\":\"Red\"},{\"name\":\"Price\",\"value\":35},{\"name\":\"Brand\",\"value\":\"Winners\"}]');
/*!40000 ALTER TABLE `data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dataschema`
--

DROP TABLE IF EXISTS `dataschema`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dataschema` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `version` bigint(20) DEFAULT '1',
  `definition` varchar(5000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_dataschema_name_version` (`name`,`version`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dataschema`
--

LOCK TABLES `dataschema` WRITE;
/*!40000 ALTER TABLE `dataschema` DISABLE KEYS */;
INSERT INTO `dataschema` VALUES (1,'Eat Chicken',2,'[{\"name\":\"Place\",\"fieldType\":4,\"enumerationId\":10},{\"name\":\"Comment\",\"fieldType\":1},{\"name\":\"Rating\",\"fieldType\":4,\"enumerationId\":11}]'),(2,'Jacket',1,'[{\"name\":\"Color\",\"fieldType\":4,\"enumerationId\":1},{\"name\":\"Price\",\"fieldType\":2},{\"name\":\"Brand\",\"fieldType\":1}]');
/*!40000 ALTER TABLE `dataschema` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enumeration`
--

DROP TABLE IF EXISTS `enumeration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enumeration` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `value` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enumeration`
--

LOCK TABLES `enumeration` WRITE;
/*!40000 ALTER TABLE `enumeration` DISABLE KEYS */;
INSERT INTO `enumeration` VALUES (1,'Color','[\"Violet\",\"Blue\",\"Green\",\"Yellow\",\"Orange\",\"Red\"]'),(2,'Gender','[\"Male\",\"Female\"]'),(3,'Car Brand','[\"Volkswagen\",\"Toyota\",\"Ford\",\"Honda\",\"Chrysler\"]'),(4,'Degree','[\"Bachelor\",\"Master\",\"Doctor\"]'),(5,'Programming Language','[\"C\",\"C++\",\"Java\",\"Python\",\"PHP\",\"JavaScript\",\"Ruby\",\"C#\"]'),(6,'Book Genre','[\"Science fiction\",\"Satire\",\"Drama\",\"Romance\",\"Mystery\",\"Fantasy\",\"History\"]'),(7,'Music','[\"Blues\",\"Classical\",\"Country\",\"Electronic\",\"Hip-hop\",\"Holiday\",\"Jazz\",\"Pop\",\"R&B\",\"Rock\"]'),(8,'Continent','[\"Asia\",\"North America\",\"South America\",\"Africa\",\"Europe\",\"Antarctic\",\"Australia\"]'),(9,'Element','[\"Hydrogen\",\"Helium\",\"Lithium\",\"Berylium\",\"Boron\",\"Carbon\",\"Nitrogen\",\"Oxygen\",\"Fluorine\",\"Neon\"]'),(10,'Fried Chicken','[\"KFC\",\"Mary Brown\'s\",\"Popeye\'s\",\"Wild Wing\",\"WingsUp!\"]'),(11,'Rating','[\"1 Star\",\"2 Star\",\"3 Star\",\"4 Star\",\"5 Star\"]');
/*!40000 ALTER TABLE `enumeration` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-22 14:32:24
