#!/bin/bash
mkdir -p ./temp/marcel-angular
cp marcel-angular/Dockerfile ./temp/marcel-angular
cd ./temp/marcel-angular
git clone git@bitbucket.org:raymondca/marcel-angular.git
docker build -t raymondchen625/marcel-angular .
cd ../..
rm -rf ./temp