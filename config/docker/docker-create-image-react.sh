#!/bin/bash
mkdir -p ./temp/marcel-react
cp marcel-react/Dockerfile ./temp/marcel-react
cd ./temp/marcel-react
git clone git@bitbucket.org:raymondca/marcel-react.git
docker build -t raymondchen625/marcel-react .
cd ../..
rm -rf ./temp