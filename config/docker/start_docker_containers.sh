#!/usr/bin/env bash
docker run --name=marcel -d -e MYSQL_ROOT_PASSWORD=marcel -e MYSQL_USER=marcel -e MYSQL_PASSWORD=marcel -e MYSQL_DATABASE=marcel raymondchen625/marcel-db
docker run --name=marcel-web -d --link marcel:latest -p 1982:1982 raymondchen625/marcel-web
docker run --name=marcel-api -d --link marcel:lastet -p 1973:1973 raymondchen625/marcel-api
docker run --name=marcel-node -d --link marcel:latest -p 1987:3000 raymondchen625/marcel-node
docker run --name=marcel-react -d --link marcel:latest -p 1998:3000 raymondchen625/marcel-react
