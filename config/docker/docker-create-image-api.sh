#!/bin/bash
mkdir -p ./temp/marcel-api
cp marcel-api/Dockerfile ./temp/marcel-api
cd ./temp/marcel-api
git clone git@bitbucket.org:raymondca/marcel.git
docker build -t raymondchen625/marcel-api .
cd ../..
rm -rf ./temp/marcel-api